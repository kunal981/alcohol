//
//  RetrogradeViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/29/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import CoreData

class RetrogradeViewController: UIViewController,UITextFieldDelegate {
    
    var calendar = NSCalendar.currentCalendar()
    var strIncident = NSString()
    var strTest = NSString()
    var dateFormatIncident = NSDateFormatter()
    var dateFormatTest = NSDateFormatter()
    
    
    @IBOutlet var incidentLbl: UILabel!
    @IBOutlet var testLbl: UILabel!
    @IBOutlet var bacLbl: UILabel!
    @IBOutlet var caseNameLbl: UILabel!
    @IBOutlet var timeOfTestTxtField: UITextField!
    
    @IBOutlet var timeOfIncidentTxtField: UITextField!
    @IBOutlet var timeOfTestBtn: UIButton!
    @IBOutlet var timeOfIncidentBtn: UIButton!
    @IBOutlet var datePicker: UIDatePicker!
    
    
    @IBOutlet var datePickerTest: UIDatePicker!
    @IBOutlet var datePickerTestView: UIView!
    @IBOutlet var datePickerView: UIView!
    @IBOutlet var resultTextField: UITextField!
    @IBOutlet var caseTextField: UITextField!
    @IBOutlet var retrogradeLbl: UILabel!
    @IBOutlet var calculateBtn: UIButton!
    @IBOutlet var timeTestView: UIView!
    @IBOutlet var timeIncidentView: UIView!
    @IBOutlet var bacResultView: UIView!
    @IBOutlet var nameView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        
        
        
        let paddingViewName = UIView(frame: CGRectMake(0, 0, 15, caseTextField.frame.size.height))
        caseTextField.leftView = paddingViewName
        caseTextField.leftViewMode = UITextFieldViewMode.Always
        
        
        let paddingViewBacResult = UIView(frame: CGRectMake(0, 0, 15, resultTextField.frame.size.height))
        resultTextField.leftView = paddingViewBacResult
        resultTextField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewIncident = UIView(frame: CGRectMake(0, 0, 15, timeOfIncidentTxtField.frame.size.height))
        timeOfIncidentTxtField.leftView = paddingViewIncident
        timeOfIncidentTxtField.leftViewMode = UITextFieldViewMode.Always
        
        
        let paddingViewTest = UIView(frame: CGRectMake(0, 0, 15, timeOfTestTxtField.frame.size.height))
        timeOfTestTxtField.leftView = paddingViewTest
        timeOfTestTxtField.leftViewMode = UITextFieldViewMode.Always
        
        
        
        
        caseTextField.delegate = self
        resultTextField.delegate = self
        
        datePickerTestView.layer.borderColor = UIColor.whiteColor().CGColor
        
        datePickerTest.setValue(UIColor.whiteColor(), forKey: "textColor")
        datePicker.setValue(UIColor.whiteColor(), forKey: "textColor")
        datePickerView.layer.borderColor = UIColor.whiteColor().CGColor
        datePickerView.backgroundColor = UIColor.blackColor()
        datePicker.backgroundColor = UIColor.blackColor()
        retrogradeLbl.layer.borderColor = UIColor.whiteColor().CGColor
        calculateBtn.layer.borderColor = UIColor.whiteColor().CGColor
        timeTestView.layer.borderColor = UIColor.whiteColor().CGColor
        
        timeIncidentView.layer.borderColor = UIColor.whiteColor().CGColor
        bacResultView.layer.borderColor = UIColor.whiteColor().CGColor
        nameView.layer.borderColor = UIColor.whiteColor().CGColor
        
        caseTextField.attributedPlaceholder = NSAttributedString(string: "John", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        resultTextField.attributedPlaceholder = NSAttributedString(string: "0.087", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        compatability()
        
    }
    
    func compatability()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            retrogradeLbl.font = UIFont.boldSystemFontOfSize(22)
            caseNameLbl.font = UIFont.systemFontOfSize(19)
            bacLbl.font = UIFont.systemFontOfSize(19)
            incidentLbl.font = UIFont.systemFontOfSize(19)
            testLbl.font = UIFont.systemFontOfSize(19)
            calculateBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(22)
            caseTextField.font = UIFont.systemFontOfSize(16)
            resultTextField.font = UIFont.systemFontOfSize(16)
            timeOfIncidentTxtField.font = UIFont.systemFontOfSize(16)
            timeOfTestTxtField.font = UIFont.systemFontOfSize(16)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        caseTextField.text = ""
        resultTextField.text = ""
        timeOfIncidentTxtField.text = "00:00"
        timeOfTestTxtField.text = "00:00"
        
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        caseTextField.resignFirstResponder()
        resultTextField.resignFirstResponder()
        
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func calculateBtn(sender: AnyObject)
    {
        if(caseTextField.text == "" || resultTextField.text == "" || timeOfIncidentTxtField.text == "00:00" || timeOfTestTxtField.text == "00:00")
        {
            var alert = UIAlertView(title: "Alert", message: "Required all field to enter", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
        }
        else
        {
            
            var componentsIncient = calendar.components((.CalendarUnitHour | .CalendarUnitMinute), fromDate: datePicker.date)
            var hourIncident = componentsIncient.hour
            println(hourIncident)
            var minuteIncident = componentsIncient.minute
            println(minuteIncident)
            
            
            var componentsTest = calendar.components((.CalendarUnitHour | .CalendarUnitMinute), fromDate: datePickerTest.date)
            var hourTest = componentsTest.hour
            println(hourTest)
            var minuteTest = componentsTest.minute
            println(minuteTest)
            
            var minuteDiff = Int()
            
            minuteDiff = (hourTest - hourIncident)*60 + minuteTest - minuteIncident
            
            println(minuteDiff)
            
            if minuteDiff > 45
                
            {
                
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                let managedContext = appDelegate.managedObjectContext!
                
                let entity = NSEntityDescription.entityForName("Bacdatabase", inManagedObjectContext: (managedContext))
                
                let person = NSManagedObject (entity:entity!, insertIntoManagedObjectContext: managedContext)
                person.setValue(NSString(format: "%@ g/dl", resultTextField.text).floatValue, forKey: "bacResult")
                person.setValue(caseTextField.text, forKey: "caseName")
                person.setValue("Retrograde Extrapolation", forKey: "averageBac")
                
                
                
                
                var error:NSError?
                if !(managedContext.save(&error))
                {
                    println("could not save \(error)")
                }
                else
                {
                    println("saveData")
                }
                
                
                condition = true
                
                strDescription = (caseTextField.text)
                bacFloat = NSString(format: "%@ g/dl", resultTextField.text).floatValue
                println(bacFloat)
                var description = storyboard?.instantiateViewControllerWithIdentifier("description") as! DescriptionCaseNameViewController
                self.navigationController?.pushViewController(description, animated: true)
                
                
            }
            else
                
            {
                
                
                stringReportLabel = caseTextField.text
                strIncidentReport = NSString(format: "I.E.  %@ (Incident)", timeOfIncidentTxtField.text!)
                strTestReport = NSString(format: "  %@ BAC test", timeOfTestTxtField.text!)
                var report = storyboard?.instantiateViewControllerWithIdentifier("report") as! AlcoholReportViewController
                self.navigationController?.pushViewController(report, animated: true)
                
                
                
            }
            
            
            
        }
        
    }
    
    @IBAction func timeOfIncident(sender: AnyObject)
    {
        
        
        datePickerView.hidden = false
        
    }
    
    
    @IBAction func timeOfTest(sender: AnyObject)
    {
        
        datePickerTestView.hidden = false
        
    }
    
    
    @IBAction func backEnd(sender: AnyObject)
    {
        datePickerView.hidden = true
        datePickerTestView.hidden = true
        caseTextField.resignFirstResponder()
        resultTextField.resignFirstResponder()
        
        
    }
    
    
    @IBAction func setTimeBtn(sender: AnyObject)
        
    {
        
        
        dateFormatIncident.dateFormat = "hh:mm:a"
        
        strIncident = dateFormatIncident.stringFromDate(datePicker.date)
        println(strIncident)
        
        self.timeOfIncidentTxtField.text = strIncident as String
        datePickerView.hidden =  true
        
    }
    
    
    @IBAction func cancelBtn(sender: AnyObject)
    {
        datePickerView.hidden = true
        
    }
    
    
    @IBAction func cancekBtnTest(sender: AnyObject)
    {
        datePickerTestView.hidden = true
    }
    @IBAction func setBtnTest(sender: AnyObject)
    {
        
        dateFormatTest.dateFormat = "hh:mm:a"
        
        strTest = dateFormatTest.stringFromDate(datePickerTest.date)
        println(strTest)
        
        if dateFormatTest.stringFromDate(datePickerTest.date)>dateFormatIncident.stringFromDate(datePicker.date)
        {
            self.timeOfTestTxtField.text = strTest as String
        }
        else
        {
            let alert = UIAlertView(title: "Alert", message: "Test time should be greater then incident time", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            
        }
        
        
        datePickerTestView.hidden =  true
        
        
        
        
    }
}








