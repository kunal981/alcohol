//
//  CasesViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/25/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import CoreData
class CasesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    
    var BACData = [NSManagedObject]()
    
    @IBOutlet var tableview: UITableView!
    @IBOutlet var casesLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            
            
            
            
            casesLabel.font = UIFont.boldSystemFontOfSize(22)
            
        }
        
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        let fetchRequest = NSFetchRequest(entityName:"Bacdatabase")
        
        var error:NSError?
        
        let fetchResults = managedContext.executeFetchRequest(fetchRequest, error:&error) as? [NSManagedObject]
        
        if let results = fetchResults
        {
            BACData = results
            
            
        }
        else
        {
            println("could not fetch \(error), \(error!.userInfo)")
        }
        self.tableview.reloadData()
        
        
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.tabBarController?.selectedIndex = 0
        
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return BACData.count
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! CustomCellTableViewCell
        
        
        cell.cellView.layer.borderColor = UIColor.whiteColor().CGColor
        let bac = BACData[indexPath.row]
        println("%@",bac.valueForKey("caseName"))
        
        cell.emailLbl.text = bac.valueForKey("averageBac") as? String
        cell.nameLbl.text = bac.valueForKey("caseName") as? String
        var bacResult: Float = bac.valueForKey("bacResult")!.floatValue!
        
        cell.bacLbl.text = NSString (format: "%.3f g/dl",bacResult) as String
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if editingStyle == UITableViewCellEditingStyle.Delete
        {
            let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            let context:NSManagedObjectContext = appdelegate.managedObjectContext!
            context.deleteObject(BACData[indexPath.row])
            
            BACData.removeAtIndex(indexPath.row)
            context.save(nil)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            
            
            
            
            
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        let bac = BACData[indexPath.row]
        var bacResult: Float = bac.valueForKey("bacResult")!.floatValue!
        
        strBACLabel = NSString(format: "%.3f g/dl", bacResult)
        strDescription = bac.valueForKey("caseName") as! String
        
        var description = storyboard?.instantiateViewControllerWithIdentifier("description") as! DescriptionCaseNameViewController
        self.navigationController?.pushViewController(description, animated: true)
        
        
    }
    
    
}
