//
//  CalculateTableViewCell.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 7/13/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class CalculateTableViewCell: UITableViewCell {

    @IBOutlet var calculateVolumeDrink: UILabel!
    @IBOutlet var calculateAlcoholPercent: UILabel!
    @IBOutlet var calculateDrinkName: UILabel!
    @IBOutlet var calculateCellView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            calculateAlcoholPercent.font = UIFont.systemFontOfSize(18)
            calculateDrinkName.font = UIFont.systemFontOfSize(18)
            calculateVolumeDrink.font = UIFont.systemFontOfSize(18)
           
            
            
        }
        
        
        
    }

}
