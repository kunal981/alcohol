//
//  DrinkEstimatorViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/29/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class DrinkEstimatorViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    var gender:NSArray = ["Male","Female"]
    
    
    var sexRatio = Float()
    var sizeHeight = UIScreen.mainScreen().bounds.height
    @IBOutlet var drinkerProfileLbl: UILabel!
    @IBOutlet var bacLbl: UILabel!
    @IBOutlet var weightLbl: UILabel!
    @IBOutlet var caseNameLbl: UILabel!
    @IBOutlet var genderTextField: UITextField!
    @IBOutlet var genderMF: UIButton!
    @IBOutlet var tableViewLet: UITableView!
    @IBOutlet var bacTextField: UITextField!
    @IBOutlet var weightTextField: UITextField!
    
    @IBOutlet var genderLbl: UILabel!
    @IBOutlet var caseNameTextField: UITextField!
    @IBOutlet var acView: UIView!
    @IBOutlet var genderView: UIView!
    @IBOutlet var weightView: UIView!
    @IBOutlet var caseNameView: UIView!
    @IBOutlet var drinkEstimatorLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if sizeHeight == 667
        {
        
            
            var line = UIView(frame: CGRectMake(30, drinkerProfileLbl.frame.origin.y-6, 100, 1))
            line.backgroundColor = UIColor.whiteColor()
            
            self.view.addSubview(line)
            
            var line2 = UIView(frame: CGRectMake(245, drinkerProfileLbl.frame.origin.y-6, 100, 1))
            line2.backgroundColor = UIColor.whiteColor()
            self.view.addSubview(line2)
        }
            
            
        else if sizeHeight == 736
        {
            var line = UIView(frame: CGRectMake(30, drinkerProfileLbl.frame.origin.y-4, 120, 1))
            line.backgroundColor = UIColor.whiteColor()
            
            self.view.addSubview(line)
            
            var line2 = UIView(frame: CGRectMake(265, drinkerProfileLbl.frame.origin.y-4, 120, 1))
            line2.backgroundColor = UIColor.whiteColor()
            self.view.addSubview(line2)
            
        }
        else if sizeHeight == 568
        {
            
            var line = UIView(frame: CGRectMake(30, drinkerProfileLbl.frame.origin.y-8, 75, 1))
            line.backgroundColor = UIColor.whiteColor()
            
            self.view.addSubview(line)
            
            var line2 = UIView(frame: CGRectMake(215, drinkerProfileLbl.frame.origin.y-8, 75, 1))
            line2.backgroundColor = UIColor.whiteColor()
            self.view.addSubview(line2)
        }
        else if sizeHeight == 480
            
        {
            var line = UIView(frame: CGRectMake(30, drinkerProfileLbl.frame.origin.y-10, 73, 1))
            line.backgroundColor = UIColor.whiteColor()
            
            self.view.addSubview(line)
            
            var line2 = UIView(frame: CGRectMake(215, drinkerProfileLbl.frame.origin.y-10, 73, 1))
            line2.backgroundColor = UIColor.whiteColor()
            self.view.addSubview(line2)
        }
        else
        {
            
            
            drinkerProfileLbl.font=UIFont.systemFontOfSize(20)
            
            var line = UIView(frame: CGRectMake(40, drinkerProfileLbl.frame.origin.y, 270, 1))
            line.backgroundColor = UIColor.whiteColor()
            
            self.view.addSubview(line)
            
            var line2 = UIView(frame: CGRectMake(460, drinkerProfileLbl.frame.origin.y, 270, 1))
            line2.backgroundColor = UIColor.whiteColor()
            self.view.addSubview(line2)
        }
        
        let paddingViewName = UIView(frame: CGRectMake(0, 0, 15, caseNameTextField.frame.size.height))
        caseNameTextField.leftView = paddingViewName
        caseNameTextField.leftViewMode = UITextFieldViewMode.Always
        
        
        let paddingViewBac = UIView(frame: CGRectMake(0, 0, 15, bacTextField.frame.size.height))
        bacTextField.leftView = paddingViewBac
        bacTextField.leftViewMode = UITextFieldViewMode.Always
        
        
        let paddingViewWeight = UIView(frame: CGRectMake(0, 0, 15, weightTextField.frame.size.height))
        weightTextField.leftView = paddingViewWeight
        weightTextField.leftViewMode = UITextFieldViewMode.Always
        
        
        
        let paddingViewGender = UIView(frame: CGRectMake(0, 0, 15, genderTextField.frame.size.height))
        genderTextField.leftView = paddingViewGender
        genderTextField.leftViewMode = UITextFieldViewMode.Always
        
        
        
        tableViewLet.frame = CGRectMake(genderView.frame.origin.x + genderLbl.frame.size.width, genderView.frame.origin.y + genderMF.frame.size.height+10, tableViewLet.frame.size.width, tableViewLet.frame.size.height)
        self.tableViewLet.hidden = true
        
        self.navigationItem.hidesBackButton = true
        bacTextField.delegate = self
        
        weightTextField.delegate = self
        caseNameTextField.delegate = self
        
        drinkEstimatorLbl.layer.borderColor = UIColor.whiteColor().CGColor
        tableViewLet.layer.borderColor = UIColor.whiteColor().CGColor
        
        acView.layer.borderColor = UIColor.whiteColor().CGColor
        genderView.layer.borderColor = UIColor.whiteColor().CGColor
        weightView.layer.borderColor = UIColor.whiteColor().CGColor
        caseNameView.layer.borderColor = UIColor.whiteColor().CGColor
        
        
        bacTextField.attributedPlaceholder = NSAttributedString(string: ".086", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        weightTextField.attributedPlaceholder = NSAttributedString(string: "12.5Lbs", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        
        
        
        caseNameTextField.attributedPlaceholder = NSAttributedString(string: "John", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            drinkEstimatorLbl.font = UIFont.boldSystemFontOfSize(22)
            caseNameLbl.font = UIFont.systemFontOfSize(19)
            genderLbl.font = UIFont.systemFontOfSize(19)
            weightLbl.font = UIFont.systemFontOfSize(19)
            bacLbl.font = UIFont.systemFontOfSize(19)
            caseNameTextField.font = UIFont.systemFontOfSize(16)
            bacTextField.font = UIFont.systemFontOfSize(16)
            weightTextField.font = UIFont.systemFontOfSize(16)
            genderTextField.font = UIFont.systemFontOfSize(16)
            
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        tableViewLet.hidden = true
        weightTextField.resignFirstResponder()
        
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if caseNameTextField.resignFirstResponder()
        {
            
            weightTextField.becomeFirstResponder()
            return false
            
        }
        if weightTextField.resignFirstResponder()
        {
            
            if weightTextField.text == ""
            {
                weightTextField.becomeFirstResponder()
            }
            
            
        }
        
        
        
        
        
        if bacTextField.resignFirstResponder()
        {
            
            
            
            
            if  caseNameTextField.text == ""
            {
                let alert = UIAlertView(title: "Alert", message: "Please enter case name", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
                weightTextField.text = ""
                caseNameTextField.becomeFirstResponder()
                
            }
            else if bacTextField.text == ""
                
            {
                let alert = UIAlertView(title: "Alert", message: "Please enter BAC value", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
                bacTextField.becomeFirstResponder()
                
            }
                
            else if weightTextField.text == ""
            {
                let alert = UIAlertView(title: "Alert", message: "Please enter case name", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
                
                caseNameTextField.becomeFirstResponder()
                
            }
                
                
            else
            {
                if genderTextField.text == "Male"
                    
                {
                    
                    sexRatio = 0.68
                }
                if genderTextField.text == "Female"
                {
                    
                    sexRatio = 0.55
                }
                
                
                var bacValue = Float()
                var weightValue = Float()
                var alcoholAmount = Float()
                var alcoholAmountInFloz  = Float()
                
                var totalDrink = Float()
                
                
                
                bacValue = (bacTextField.text as NSString).floatValue
                
                println(bacValue)
                
                weightValue = (weightTextField.text as NSString).floatValue
                
                println(weightValue)
                
                alcoholAmount =   (bacValue * (weightValue * 454) * 0.68)/78.9
                
                alcoholAmountInFloz = alcoholAmount/29.6
                
                totalDrink = alcoholAmountInFloz/0.6
                
                
                if totalDrink > 0.25
                {
                    if totalDrink < 1
                    {
                        
                        let resultFomat = NSString(format: "%.3f Total Number of Drinks", totalDrink)
                        
                        
                        let alert = UIAlertView(title: "", message: resultFomat as String , delegate: self, cancelButtonTitle: "Ok")
                        
                        alert.show()
                    }
                    else
                        
                    {
                        var intDrink = NSString(format: "%i Total Number of Drinks", Int(totalDrink))
                        println(intDrink)
                        let alert = UIAlertView(title: "", message: intDrink as? String  , delegate: self, cancelButtonTitle: "Ok")
                        
                        alert.show()
                    }
                    
                }
                else
                    
                {
                    
                    let alert = UIAlertView(title: "", message: "0.25 Total Number of Drinks" , delegate: self, cancelButtonTitle: "Ok")
                    
                    alert.show()
                    
                    
                    
                }
                weightTextField.text = ""
                bacTextField.text = ""
                
                
                
                
            }
            
            
            
            
        }
        
        
        
        return true
    }
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func genderBtn(sender: AnyObject)
    {
        self.tableViewLet.hidden = false
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return gender.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
        
        cell.textLabel?.text = gender[indexPath.row] as? String
        cell.textLabel?.textColor = UIColor.whiteColor()
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            cell.backgroundColor = UIColor.blackColor()
        }
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var str: AnyObject = gender[indexPath.row]
        
        genderTextField.text = str as? String
        
        self.tableViewLet.hidden = true
        
    }
    
    
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        if textField == weightTextField
        {
            
            if weightTextField.text.isEmpty
            {
                weightTextField.text = ""
                
            }
                
            else
            {
                weightTextField.text = NSString(format: "%@ lbs.", weightTextField.text) as String
            }
        }
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        
        if textField == bacTextField
        {
            self.view.frame.origin.y -= 100
            
        }
        else
        {
            
            self.view.frame.origin.y = self.view.frame.origin.y
        }
        
        return true
    }
    
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        
        
        if textField == bacTextField
        {
            
            
            self.view.frame.origin.y += 100
            
            
        }
        
        return true
    }
    
    
    
    
    
}

