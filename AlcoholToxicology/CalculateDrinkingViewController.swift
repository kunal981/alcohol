//
//  CalculateDrinkingViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/26/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import CoreData

var arraytitle = NSMutableArray()
var strWeight = NSString() 
var stringLabelCalculate = NSString()
var r = Float()

class CalculateDrinkingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate
{
    
    var hoursArray = ["0 hour","0.5 hour","1 hour","1.5 hour","2 hour","2.5 hour","3 hour","3.5 hour","4 hour","4.5 hour","5 hour","5.5 hour","6 hour","6.5 hour","7 hour","7.5 hour","8 hour","8.5 hour","9 hour","9.5 hour","10 hour"]
    
    
    @IBOutlet var customDrinkVolumeDrinkLbl: UILabel!
    @IBOutlet var customDrinkPercentAlcoholLbl: UILabel!
    @IBOutlet var customDrinkNameLbl: UILabel!
    @IBOutlet var customDrinkTitlelbl: UILabel!
    @IBOutlet var customDrinkTableTitle: UILabel!
    @IBOutlet var durationOfDrinkLbl: UILabel!
    @IBOutlet var customViewTableCancelBtn: UIButton!
    @IBOutlet var customViewTableCreateBtn: UIButton!
    
    var customViewTableData = [NSManagedObject]()
    
    @IBOutlet var customDrinkTableView: UITableView!
    @IBOutlet var customDrinkVolumeTxt: UITextField!
    var bac = Float()
    
    @IBOutlet var customViewTable: UIView!
    @IBOutlet var customDrinkPercentAlcoholTxt: UITextField!
    @IBOutlet var customDrinkNameTxt: UITextField!
    @IBOutlet var customDrinkCreateBtn: UIButton!
    @IBOutlet var customDrinkCancelBtn: UIButton!
    @IBOutlet var customDrinkView: UIView!
    @IBOutlet var frameTableView: UIView!
    @IBOutlet var smallTableViewLet: UITableView!
    
    @IBOutlet var customVolumeDrinkView: UIView!
    @IBOutlet var customDrinkPercentAlcoholView: UIView!
    @IBOutlet var customDrinkNameView: UIView!
    var ALcoholDoseACofDrinks = Float()
    
    var alcoholDose = Float()
    
    
    var acOfDrink = NSMutableArray()
    
    var hourValue :Float!
    
    var volumeDrink = NSMutableArray()
    
    @IBOutlet var btnHours: UIButton!
    
    @IBOutlet var hourLabel: UILabel!
    
    @IBOutlet var viewInTable: UIView!
    @IBOutlet var calculateBtnLbl: UIButton!
    @IBOutlet var tableViewLet: UITableView!
    
    @IBOutlet var typeAlcohol5Btn: UIButton!
    @IBOutlet var typeAlcohol4Btn: UIButton!
    @IBOutlet var typeAlcohol3Btn: UIButton!
    @IBOutlet var typeAlcohol1Btn: UIButton!
    @IBOutlet var typeAlcohol2Btn: UIButton!
    @IBOutlet var caseNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var sizeHeight = UIScreen.mainScreen().bounds.height
        
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            typeAlcohol2Btn.frame = CGRectMake(typeAlcohol2Btn.frame.origin.x, typeAlcohol2Btn.frame.origin.y, 100, 110)
            typeAlcohol2Btn.layer.cornerRadius = typeAlcohol2Btn.frame.size.width/2
            typeAlcohol2Btn.layer.masksToBounds = true
            
            typeAlcohol1Btn.frame = CGRectMake(typeAlcohol1Btn.frame.origin.x, typeAlcohol1Btn.frame.origin.y, 100, 110)
            typeAlcohol1Btn.layer.cornerRadius = typeAlcohol1Btn.frame.size.width/2
            typeAlcohol1Btn.layer.masksToBounds = true
            
            typeAlcohol3Btn.frame = CGRectMake(typeAlcohol3Btn.frame.origin.x, typeAlcohol3Btn.frame.origin.y, 100, 110)
            typeAlcohol3Btn.layer.cornerRadius = typeAlcohol3Btn.frame.size.width/2
            typeAlcohol3Btn.layer.masksToBounds = true
            
            typeAlcohol4Btn.frame = CGRectMake(typeAlcohol4Btn.frame.origin.x, typeAlcohol4Btn.frame.origin.y, 100, 110)
            typeAlcohol4Btn.layer.cornerRadius = typeAlcohol4Btn.frame.size.width/2
            typeAlcohol4Btn.layer.masksToBounds = true
            
            
            typeAlcohol5Btn.frame = CGRectMake(typeAlcohol5Btn.frame.origin.x, typeAlcohol5Btn.frame.origin.y, 100, 110)
            typeAlcohol5Btn.layer.cornerRadius = typeAlcohol5Btn.frame.size.width/2
            typeAlcohol5Btn.layer.masksToBounds = true
            
            
            caseNameLabel.font = UIFont.boldSystemFontOfSize(22)
            durationOfDrinkLbl.font = UIFont.systemFontOfSize(18)
            hourLabel.font = UIFont.systemFontOfSize(18)
            calculateBtnLbl.titleLabel?.font = UIFont.boldSystemFontOfSize(22)
            customDrinkTableTitle.font = UIFont.systemFontOfSize(18)
            customViewTableCreateBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(19)
            customViewTableCancelBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(19)
            
            
            customDrinkTitlelbl.font = UIFont.systemFontOfSize(18)
            customDrinkNameLbl.font = UIFont.systemFontOfSize(18)
            customDrinkPercentAlcoholLbl.font = UIFont.systemFontOfSize(18)
            customDrinkVolumeDrinkLbl.font = UIFont.systemFontOfSize(18)
            
            customDrinkCreateBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(19)
            customDrinkCancelBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(19)
            
            customDrinkNameTxt.font = UIFont.systemFontOfSize(16)
            customDrinkPercentAlcoholTxt.font = UIFont.systemFontOfSize(16)
            customDrinkVolumeTxt.font = UIFont.systemFontOfSize(16)
            
        }
        else
        {
            
            if sizeHeight == 480.00
            {
                typeAlcohol1Btn.frame = CGRectMake(typeAlcohol1Btn.frame.origin.x, typeAlcohol1Btn.frame.origin.y-10, 45, 55)
                typeAlcohol1Btn.layer.cornerRadius = typeAlcohol1Btn.frame.size.width/2
                typeAlcohol1Btn.layer.masksToBounds = true
                
                typeAlcohol2Btn.frame = CGRectMake(typeAlcohol2Btn.frame.origin.x, typeAlcohol2Btn.frame.origin.y-10, 45, 55)
                typeAlcohol2Btn.layer.cornerRadius = typeAlcohol2Btn.frame.size.width/2
                typeAlcohol2Btn.layer.masksToBounds = true
                
                typeAlcohol3Btn.frame = CGRectMake(typeAlcohol3Btn.frame.origin.x, typeAlcohol3Btn.frame.origin.y-10, 45, 55)
                typeAlcohol3Btn.layer.cornerRadius = typeAlcohol3Btn.frame.size.width/2
                typeAlcohol3Btn.layer.masksToBounds = true
                
                typeAlcohol4Btn.frame = CGRectMake(typeAlcohol4Btn.frame.origin.x, typeAlcohol4Btn.frame.origin.y-10, 45, 55)
                typeAlcohol4Btn.layer.cornerRadius = typeAlcohol4Btn.frame.size.width/2
                typeAlcohol4Btn.layer.masksToBounds = true
                
                typeAlcohol5Btn.frame = CGRectMake(typeAlcohol5Btn.frame.origin.x, typeAlcohol5Btn.frame.origin.y-10, 45, 55)
                typeAlcohol5Btn.layer.cornerRadius = typeAlcohol5Btn.frame.size.width/2
                typeAlcohol5Btn.layer.masksToBounds = true
                
                
                
            }
            else
            {
                
                typeAlcohol2Btn.layer.cornerRadius = typeAlcohol2Btn.frame.size.width/2
                typeAlcohol2Btn.layer.masksToBounds = true
                
                typeAlcohol1Btn.layer.cornerRadius = typeAlcohol1Btn.frame.size.width/2
                typeAlcohol1Btn.layer.masksToBounds = true
                
                typeAlcohol3Btn.layer.cornerRadius = typeAlcohol3Btn.frame.size.width/2
                typeAlcohol3Btn.layer.masksToBounds = true
                
                typeAlcohol4Btn.layer.cornerRadius = typeAlcohol4Btn.frame.size.width/2
                typeAlcohol4Btn.layer.masksToBounds = true
                
                typeAlcohol5Btn.layer.cornerRadius = typeAlcohol5Btn.frame.size.width/2
                typeAlcohol5Btn.layer.masksToBounds = true
                
            }
            
        }
        
        
        let paddingViewDrinkName = UIView(frame: CGRectMake(0, 0, 15, customDrinkNameTxt.frame.size.height))
        customDrinkNameTxt.leftView = paddingViewDrinkName
        customDrinkNameTxt.leftViewMode = UITextFieldViewMode.Always
        
        
        let paddingViewPercentAlcohol = UIView(frame: CGRectMake(0, 0, 15, customDrinkPercentAlcoholTxt.frame.size.height))
        customDrinkPercentAlcoholTxt.leftView = paddingViewPercentAlcohol
        customDrinkPercentAlcoholTxt.leftViewMode = UITextFieldViewMode.Always
        
        
        let paddingViewVolume = UIView(frame: CGRectMake(0, 0, 15, customDrinkVolumeTxt.frame.size.height))
        customDrinkVolumeTxt.leftView = paddingViewVolume
        customDrinkVolumeTxt.leftViewMode = UITextFieldViewMode.Always
        
        
        
        
        
        self.customDrinkTableView.reloadData()
        customDrinkVolumeTxt.delegate = self
        customDrinkPercentAlcoholTxt.delegate = self
        customDrinkNameTxt.delegate = self
        customDrinkView.frame = CGRectMake(0, typeAlcohol1Btn.frame.origin.y, view.frame.size.width, 378)
        customViewTable.frame = CGRectMake(0, typeAlcohol1Btn.frame.origin.y, view.frame.size.width, 378)
        customViewTable.layer.borderColor = UIColor.whiteColor().CGColor
        customViewTableCreateBtn.layer.borderColor = UIColor.whiteColor().CGColor
        customViewTableCancelBtn.layer.borderColor = UIColor.whiteColor().CGColor
        
        customDrinkView.layer.borderColor = UIColor.whiteColor().CGColor
        customVolumeDrinkView.layer.borderColor = UIColor.whiteColor().CGColor
        customDrinkPercentAlcoholView.layer.borderColor = UIColor.whiteColor().CGColor
        customDrinkNameView.layer.borderColor = UIColor.whiteColor().CGColor
        customDrinkCreateBtn.layer.borderColor = UIColor.whiteColor().CGColor
        customDrinkCancelBtn.layer.borderColor = UIColor.whiteColor().CGColor
        
        smallTableViewLet.layer.borderColor = UIColor.grayColor().CGColor
        
        caseNameLabel.text = stringLabelCalculate as String
        
        self.navigationItem.hidesBackButton = true
        
        calculateBtnLbl.layer.borderColor = UIColor.whiteColor().CGColor
        
        viewInTable.layer.borderColor = UIColor.whiteColor().CGColor
        
        
        
        caseNameLabel.layer.borderColor = UIColor.whiteColor().CGColor
        
        
        typeAlcohol1Btn.layer.borderColor = UIColor.whiteColor().CGColor
        
        
        
        typeAlcohol2Btn.layer.borderColor = UIColor.whiteColor().CGColor
        typeAlcohol3Btn.layer.borderColor = UIColor.whiteColor().CGColor
        typeAlcohol4Btn.layer.borderColor = UIColor.whiteColor().CGColor
        typeAlcohol5Btn.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    
    
    @IBAction func hourBtn(sender: AnyObject)
    {
        
        println ("\(tableViewLet.frame.size.height)")
        
        var tableMaxHeight = self.view.frame.height-tableViewLet.frame.origin.y
        var dropDownMaxY = Int(tableMaxHeight-frameTableView.frame.size.height)
        var tableCurrentHeight = arraytitle.count*44
        
        if(tableCurrentHeight <= dropDownMaxY)
            
        {
            smallTableViewLet.frame = CGRectMake(smallTableViewLet.frame.origin.x,frameTableView.frame.origin.y+15  , smallTableViewLet.frame.size.width, smallTableViewLet.frame.size.height)
        }
        else
        {
            smallTableViewLet.frame = CGRectMake(smallTableViewLet.frame.origin.x,tableViewLet.frame.size.height - frameTableView.frame.size.height+15  , smallTableViewLet.frame.size.width, smallTableViewLet.frame.size.height)
            
        }
        
        
        
        smallTableViewLet.hidden = false
        
    }
    
    
    override func viewWillAppear(animated: Bool)
        
    {
        
        
        alcoholDose = 0
        arraytitle.removeAllObjects()
        acOfDrink.removeAllObjects()
        volumeDrink.removeAllObjects()
        tableViewLet.reloadData()
        self.customDrinkTableView.reloadData()
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if tableView == tableViewLet
        {
            
            return 1
        }
            
        else if tableView == smallTableViewLet
        {
            return 1
            
        }
        else
        {
            return 1
        }
        
        
        
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if tableView == tableViewLet
        {
            
            return arraytitle.count
            
        }
            
            
        else if tableView == smallTableViewLet
        {
            return hoursArray.count
            
        }
        else
        {
            
            return customViewTableData.count
        }
        
        
        
        
        
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = CalculateTableViewCell()
        
        
        if tableView == tableViewLet
        {
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
            {
                tableViewLet.rowHeight = 80
                
            }
            
            
            
            cell = tableView.dequeueReusableCellWithIdentifier("calculateCell", forIndexPath: indexPath) as! CalculateTableViewCell
            cell.calculateDrinkName.text = arraytitle[indexPath.row] as? String
            cell.calculateAlcoholPercent.text = acOfDrink[indexPath.row] as? String
            cell.calculateVolumeDrink.text = volumeDrink[indexPath.row] as? String
            cell.calculateCellView.layer.borderColor = UIColor.whiteColor().CGColor
            
            return cell
        }
            
        else if tableView == smallTableViewLet
        {
            
            
            let cell2 = tableView.dequeueReusableCellWithIdentifier("cell2", forIndexPath: indexPath)  as! UITableViewCell
            
            cell2.textLabel?.text = hoursArray[indexPath.row]
            cell2.textLabel?.textColor = UIColor.whiteColor()
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
            {
                
                cell2.backgroundColor = UIColor.blackColor()
                
            }
            
            return cell2
            
        }
            
        else
            
        {
            let customDrinkCell = tableView.dequeueReusableCellWithIdentifier("customDrinkCell", forIndexPath: indexPath) as! CustomDrinkTableViewCell
            
            let data = customViewTableData[indexPath.row]
            
            
            
            
            
            customDrinkCell.addBtn.tag = indexPath.row
            
            customDrinkCell.customDrink.text = data.valueForKey("drinkName") as? String
            
            let alcoholPercent : Float = data.valueForKey("alcoholPercent")!.floatValue!
            
            customDrinkCell.customDrinkAlcoholPercent.text = NSString(format: "%.1f", alcoholPercent) as String
            customDrinkCell.customDrinkVolumeOfDrink.text = data.valueForKey("volumeDrink") as? String
            customDrinkCell.customDrinkCellView.layer.borderColor = UIColor.whiteColor().CGColor
            return customDrinkCell
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if tableView == tableViewLet
        {
            
            if editingStyle == UITableViewCellEditingStyle.Delete
            {
                arraytitle.removeObjectAtIndex(indexPath.row)
                acOfDrink.removeObjectAtIndex(indexPath.row)
                volumeDrink.removeObjectAtIndex(indexPath.row)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                
                
                tableView .reloadData()
            }
        }
        else
        {
            if editingStyle == UITableViewCellEditingStyle.Delete
            {
                customViewTableData.removeAtIndex(indexPath.row)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                
            }
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if tableView == smallTableViewLet
        {
            
            hourLabel.text = hoursArray[indexPath.row]
            println("....\(hourLabel.text)")
            hourValue = (hourLabel.text as! NSString).floatValue
            println("....\(hourValue)")
            smallTableViewLet.hidden = true
            
        }
        
        
    }
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func WineBtn(sender: AnyObject)
    {
        
        
        
        arraytitle.addObject("Wine")
        acOfDrink.addObject("12.0per")
        volumeDrink.addObject("5.0 fl.oz")
        ALcoholDoseACofDrinks = 12.0/100
        
        println("\(ALcoholDoseACofDrinks)")
        
        var AlcoholDoseVolumeOfWineDrinks = Float()
        
        AlcoholDoseVolumeOfWineDrinks = AlcoholDoseVolumeOfWineDrinks+5.0
        
        alcoholDose = (AlcoholDoseVolumeOfWineDrinks * 29.6) *  (ALcoholDoseACofDrinks * 0.789)+alcoholDose as Float
        println("Wine==\(alcoholDose)")
        
        tableViewLet.reloadData()
        smallTableViewLet.hidden = true
        
    }
    
    @IBAction func beerBtn(sender: AnyObject)
    {
        
        arraytitle.addObject("Beer")
        acOfDrink.addObject("5.0per")
        volumeDrink.addObject("12.0 fl.oz")
        ALcoholDoseACofDrinks = 5.0/100
        
        println("\(ALcoholDoseACofDrinks)")
        
        var AlcoholDoseVolumeOfBeerDrinks = Float()
        
        AlcoholDoseVolumeOfBeerDrinks = AlcoholDoseVolumeOfBeerDrinks+12.0
        
        alcoholDose = (AlcoholDoseVolumeOfBeerDrinks * 29.6) *  (ALcoholDoseACofDrinks * 0.789) + alcoholDose as Float
        
        println("beer==\(alcoholDose)")
        
        tableViewLet.reloadData()
        smallTableViewLet.hidden = true
        
        
    }
    
    
    @IBAction func maltBtn(sender: AnyObject)
    {
        
        arraytitle.addObject("Malt")
        acOfDrink.addObject("7.0per")
        volumeDrink.addObject("9.0 fl.oz")
        ALcoholDoseACofDrinks = 7.0/100
        
        
        
        var AlcoholDoseVolumeOfMaltsDrinks = Float()
        
        AlcoholDoseVolumeOfMaltsDrinks = AlcoholDoseVolumeOfMaltsDrinks+9.0
        alcoholDose = (AlcoholDoseVolumeOfMaltsDrinks * 29.6) *  (ALcoholDoseACofDrinks * 0.789)+alcoholDose as Float
        
        tableViewLet.reloadData()
        smallTableViewLet.hidden = true
    }
    @IBAction func ProofBtn(sender: AnyObject)
    {
        
        arraytitle.addObject("80-Proof")
        acOfDrink.addObject("40.0per")
        volumeDrink.addObject("1.5 fl.oz")
        ALcoholDoseACofDrinks = 40.0/100
        
        
        
        var AlcoholDoseVolumeOfProofDrinks = Float()
        
        AlcoholDoseVolumeOfProofDrinks = AlcoholDoseVolumeOfProofDrinks+1.5
        alcoholDose = (AlcoholDoseVolumeOfProofDrinks * 29.6) *  (ALcoholDoseACofDrinks * 0.789)+alcoholDose as Float
        
        tableViewLet.reloadData()
        smallTableViewLet.hidden = true
    }
    
    
    @IBAction func customBtn(sender: AnyObject)
    {
        
        fetchCustomDrink()
        
        self.customDrinkTableView.reloadData()
        
        customViewTable.hidden = false
        customDrinkView.hidden = true
        
        
        customViewTable.hidden = false
        smallTableViewLet.hidden = true
        
        
    }
    
    
    
    @IBAction func calculateBtn(sender: AnyObject)
    {
        
        
        
        var floatWeight = Float()
        
        
        if arraytitle.count > 0
        {
            
            
            if hourLabel.text == "0 hour"
            {
                
                
                floatWeight = (strWeight).floatValue
                
                println("\(floatWeight)")
                println("\(alcoholDose)")
                
                bac = Float(alcoholDose) / ((floatWeight*454)*r) *  100 as Float
                
                strBACLabel = NSString(format: "%.3f g/dl", bac)
                
                println("............\(bac)")
                
            }
                
            else
            {
                
                
                floatWeight = (strWeight).floatValue
                
                println("\(floatWeight)")
                println("\(alcoholDose)")
                
                bac = (Float(alcoholDose) / ((floatWeight*454)*r) *  100) - (hourValue * 0.015) as Float
                
                strBACLabel = NSString(format: "%.3f g/dl", bac)
                
                println("\(bac)")
                
                
            }
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext!
            
            let entity = NSEntityDescription.entityForName("Bacdatabase", inManagedObjectContext: (managedContext))
            
            let person = NSManagedObject (entity:entity!, insertIntoManagedObjectContext: managedContext)
            person.setValue(bac, forKey: "bacResult")
            person.setValue(caseNameLabel.text, forKey: "caseName")
            person.setValue("Average Maximum BAC", forKey: "averageBac")
            
            
            
            
            var error:NSError?
            if !(managedContext.save(&error))
            {
                println("could not save \(error)")
            }
            else
            {
                println("saveData")
            }
            
            var description = storyboard?.instantiateViewControllerWithIdentifier("description") as! DescriptionCaseNameViewController
            self.navigationController?.pushViewController(description, animated: true)
            
            
        }
        else
        {
            
            let alert = UIAlertView(title: "Alert", message: "Select atleast one Drink!", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
            
        }
        
        
        
        smallTableViewLet.hidden = true
        
        
        
        
    }
    
    
    
    @IBAction func cancelBtnCustomDrink(sender: AnyObject) {
        
        customDrinkView.hidden = true
        customViewTable.hidden = false
    }
    
    
    @IBAction func customDrinkCreateBtn(sender: AnyObject)
        
    {
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        
        if customDrinkNameTxt.text == "" || customDrinkPercentAlcoholTxt.text == "" || customDrinkVolumeTxt.text == ""
        {
            let alert = UIAlertView(title: "Alert", message:"All Fields value require" , delegate: self, cancelButtonTitle: "Ok")
            
            alert.show()
        }
            
        else
        {
            
            
            let entity = NSEntityDescription.entityForName("Customdrink", inManagedObjectContext: (managedContext))
            
            let person = NSManagedObject (entity:entity!, insertIntoManagedObjectContext: managedContext)
            person.setValue(customDrinkNameTxt.text, forKey: "drinkName")
            person.setValue(NSString(format: "%@", customDrinkPercentAlcoholTxt.text).floatValue , forKey: "alcoholPercent")
            person.setValue(NSString(format: "%@.0 fl.oz", customDrinkVolumeTxt.text), forKey: "volumeDrink")
            
            
            
            
            var error:NSError?
            if !(managedContext.save(&error))
            {
                println("could not save \(error)")
            }
            else
            {
                println("saveData")
            }
        }
        
        fetchCustomDrink()
        
        self.customDrinkTableView.reloadData()
        
        customViewTable.hidden = false
        customDrinkView.hidden = true
        
    }
    
    func fetchCustomDrink()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        
        let fetchRequest = NSFetchRequest(entityName:"Customdrink")
        
        var error:NSError?
        
        let fetchResults = managedContext.executeFetchRequest(fetchRequest, error:&error) as? [NSManagedObject]
        
        if let results = fetchResults
        {
            customViewTableData = results
            
            
        }
        else
        {
            println("could not fetch \(error), \(error!.userInfo)")
        }
    }
    
    
    @IBAction func backEnd(sender: AnyObject)
    {
        smallTableViewLet.hidden = true
    }
    
    @IBAction func customCreateBtnTable(sender: AnyObject)
    {
        
        
        
        customDrinkView.hidden = false
        customViewTable.hidden = true
        
        
    }
    
    @IBAction func customViewTableCancelBtn(sender: AnyObject)
    {
        customDrinkView.hidden = true
        customViewTable.hidden = true
    }
    
    @IBAction func addActionBtn(sender: AnyObject)
    {
        arraytitle.addObject(customViewTableData[sender.tag].valueForKey("drinkName") as! String)
        println("\(arraytitle)")
        
        
        acOfDrink.addObject(NSString(format: "%.1fper", customViewTableData[sender.tag].valueForKey("alcoholPercent")!.floatValue!))
        println("\(acOfDrink)")
        
        volumeDrink.addObject(customViewTableData[sender.tag].valueForKey("volumeDrink") as! String)
        
        
        for  acDrink in acOfDrink
            
        {
            ALcoholDoseACofDrinks = (acDrink as! NSString).floatValue/100
            println(ALcoholDoseACofDrinks)
        }
        
        
        var volumeOfDrink = Float()
        
        for volume in volumeDrink
        {
            volumeOfDrink =  (volume as! NSString).floatValue
            println(volumeOfDrink)
        }
        
        
        
        alcoholDose = (volumeOfDrink * 29.6) *  (ALcoholDoseACofDrinks * 0.789) + alcoholDose   as Float
        
        println(alcoholDose)
        
        
        
        tableViewLet.reloadData()
        customViewTable.hidden = true
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        customDrinkNameTxt.resignFirstResponder()
        customDrinkPercentAlcoholTxt.resignFirstResponder()
        customDrinkVolumeTxt.resignFirstResponder()
        smallTableViewLet.hidden = true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
}
