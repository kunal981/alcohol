//
//  ConsultViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/29/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import MessageUI
class ConsultViewController: UIViewController,UITextViewDelegate,UITextFieldDelegate,MFMailComposeViewControllerDelegate {
    
    @IBOutlet var messageLbl: UILabel!
    @IBOutlet var toLbl: UILabel!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var consultTxtViewTitle: UITextView!
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var sendBtn: UIButton!
    @IBOutlet var messageTxtView: UITextView!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var consultLbl: UILabel!
    
    @IBOutlet var consultView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let paddingViewName = UIView(frame: CGRectMake(0, 0, 15, nameTextField.frame.size.height))
        nameTextField.leftView = paddingViewName
        nameTextField.leftViewMode = UITextFieldViewMode.Always
        
        
        let paddingViewEmail = UIView(frame: CGRectMake(0, 0, 15, emailTxtField.frame.size.height))
        emailTxtField.leftView = paddingViewEmail
        emailTxtField.leftViewMode = UITextFieldViewMode.Always
        
        
        nameTextField.delegate = self
        emailTxtField.delegate = self
        messageTxtView.delegate = self
        
        
        
        cancelBtn.layer.borderColor = UIColor.whiteColor().CGColor
        sendBtn.layer.borderColor = UIColor.whiteColor().CGColor
        consultView.layer.borderColor = UIColor.whiteColor().CGColor
        messageTxtView.layer.borderColor = UIColor.whiteColor().CGColor
        emailTxtField.layer.borderColor = UIColor.whiteColor().CGColor
        nameTextField.layer.borderColor = UIColor.whiteColor().CGColor
        nameTextField.attributedPlaceholder = NSAttributedString(string: "Enter your name", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        emailTxtField.attributedPlaceholder = NSAttributedString(string: "Enter your Email Address", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        compatability()
        
    }
    
    
    func compatability()
    {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
            
        {
            
            nameLbl.font = UIFont.systemFontOfSize(18)
            toLbl.font = UIFont.systemFontOfSize(18)
            messageLbl.font = UIFont.systemFontOfSize(18)
            sendBtn.titleLabel?.font=UIFont.boldSystemFontOfSize(18)
            cancelBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(18)
            consultTxtViewTitle.font = UIFont.systemFontOfSize(19)
            consultLbl.font = UIFont.boldSystemFontOfSize(22)
            nameTextField.font = UIFont.systemFontOfSize(16)
            emailTxtField.font = UIFont.systemFontOfSize(16)
            messageTxtView.font = UIFont.systemFontOfSize(16)
            
            
            
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    func textViewDidBeginEditing(textView: UITextView)
    {
        if textView.text == "Enter your message"
        {
            textView.text = ""
        }
    }
    
    
    func textViewDidEndEditing(textView: UITextView)
    {
        if textView.text == ""
        {
            textView.text = "Enter your message"
        }
        
    }
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.tabBarController?.selectedIndex = 0
    }
    
    
    
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent)
    {
        messageTxtView.resignFirstResponder()
        nameTextField.resignFirstResponder()
        emailTxtField.resignFirstResponder()
        
        
    }
    
    
    
    
    @IBAction func sendBtn(sender: AnyObject)
    {
        if (nameTextField.text == "" || emailTxtField.text == "" || messageTxtView.text == "")
        {
            var alert = UIAlertView(title: "Alert", message: "Enter detail first", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
        else
        {
            var emailTitle = ""
            var messageBody = messageTxtView.text
            var toRecipents = [emailTxtField.text]
            var mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setMessageBody(messageBody, isHTML: false)
            mc.setToRecipients(toRecipents)
            
            self.presentViewController(mc, animated: true, completion: nil)
        }
    }
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError) {
        var refreshAlert1 = UIAlertController(title: "Mail sent", message: "You request has been sent successfully", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert1.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
            //println("Handle Ok logic here")
        }))
        
        var refreshAlert2 = UIAlertController(title: "Mail couldn't sent", message: "Your request could not be processed", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert2.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
            //println("Handle Ok logic here")
        }))
        var showAlert = true
        var alertView: UIAlertController!
        
        switch result.value {
        case MFMailComposeResultCancelled.value:
            println("Mail cancelled")
            showAlert = false
        case MFMailComposeResultSaved.value:
            println("Mail saved")
            showAlert = false
        case MFMailComposeResultSent.value:
            alertView = refreshAlert1
        case MFMailComposeResultFailed.value:
            alertView = refreshAlert2
        default:
            showAlert = false
            break
        }
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if showAlert
        {
            presentViewController(alertView, animated: true, completion: nil)
        }
        
        
        
        
    }
    
    
    @IBAction func cancelBtn(sender: AnyObject)
    {
        //        nameTextField.text = ""
        //        emailTxtField.text = ""
        //        messageTxtView.text = ""
        //
    }
    
    
}
