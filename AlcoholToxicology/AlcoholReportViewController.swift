//
//  AlcoholReportViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/30/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

var strIncidentReport = NSString()
var strTestReport = NSString()
var stringReportLabel = NSString()
class AlcoholReportViewController: UIViewController {
    
    @IBOutlet var testLbl: UILabel!
    @IBOutlet var incidentLbl: UILabel!
    @IBOutlet var textView: UITextView!
    @IBOutlet var reportNameLbl: UILabel!
    @IBOutlet var reportLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        incidentLbl.text = strIncidentReport as String
        testLbl.text = strTestReport as String
        reportNameLbl.text = stringReportLabel as? String
        textView.layer.borderColor = UIColor.whiteColor().CGColor
        reportLbl.layer.borderColor = UIColor.whiteColor().CGColor
        reportNameLbl.layer.borderColor = UIColor.whiteColor().CGColor
        self.navigationItem.hidesBackButton = true
        
        compatability()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func compatability()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
        
        reportLbl.font = UIFont.systemFontOfSize(22)
        reportNameLbl.font = UIFont.boldSystemFontOfSize(22)
        incidentLbl.font = UIFont.systemFontOfSize(18)
        testLbl.font = UIFont.systemFontOfSize(18)
        textView.font = UIFont.systemFontOfSize(22)
        textView.frame.size.height = 300
        }
        
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
