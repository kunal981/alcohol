//
//  ShareViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/25/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController {
    
    @IBOutlet var pinterestImage: UIImageView!
    @IBOutlet var twitterImage: UIImageView!
    @IBOutlet var facebookImage: UIImageView!
    @IBOutlet var linkedinImage: UIImageView!
    @IBOutlet var shareLabel: UILabel!
    @IBOutlet var pinterestView: UIView!
    @IBOutlet var twitterView: UIView!
    @IBOutlet var facebookView: UIView!
    @IBOutlet var linkedinView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pinterestView.layer.borderColor = UIColor.grayColor().CGColor
        
        twitterView.layer.borderColor = UIColor.grayColor().CGColor
        facebookView.layer.borderColor = UIColor.grayColor().CGColor
        linkedinView.layer.borderColor = UIColor.grayColor().CGColor
        shareLabel.layer.borderColor = UIColor.whiteColor().CGColor
        pinterestImage.layer.borderColor = UIColor.whiteColor().CGColor
        twitterImage.layer.borderColor = UIColor.whiteColor().CGColor
        facebookImage.layer.borderColor = UIColor.whiteColor().CGColor
        linkedinImage.layer.borderColor = UIColor.whiteColor().CGColor
        
        linkedinImage.layer.cornerRadius = linkedinImage.frame.size.width/2
        
        twitterImage.layer.cornerRadius = twitterImage.frame.size.width/2
        
        facebookImage.layer.cornerRadius = facebookImage.frame.size.width/2
        pinterestImage.layer.cornerRadius = pinterestImage.frame.size.width/2
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.tabBarController?.selectedIndex = 0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    
}
