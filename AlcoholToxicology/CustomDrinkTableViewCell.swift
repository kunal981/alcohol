//
//  CustomDrinkTableViewCell.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 7/13/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class CustomDrinkTableViewCell: UITableViewCell {
    
    @IBOutlet var addBtn: UIButton!
    @IBOutlet var customDrinkVolumeOfDrink: UILabel!
    @IBOutlet var customDrink: UILabel!
    @IBOutlet var customDrinkAlcoholPercent: UILabel!
    @IBOutlet var customDrinkCellView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            customDrink.font = UIFont.systemFontOfSize(18)
            customDrinkAlcoholPercent.font = UIFont.systemFontOfSize(18)
            customDrinkVolumeOfDrink.font = UIFont.systemFontOfSize(18)
            
            addBtn.frame = CGRectMake(addBtn.frame.origin.x, addBtn.frame.origin.y, 30, 30)
            
            
            
        }
        
    }
    
}
