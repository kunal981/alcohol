//
//  FactsViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/25/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class FactsViewController: UIViewController {
    var sectionTitleArray : NSMutableArray = NSMutableArray()
    var sectionContentDict : NSMutableDictionary = NSMutableDictionary()
    var arrayForBool : NSMutableArray = NSMutableArray()
    
    @IBOutlet var titleLblFacts: UILabel!
    @IBOutlet var tableViewLet: UITableView!
    var questionAlcohol = NSArray()
    @IBOutlet var factsDrLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
            
        {
            titleLblFacts.font = UIFont.boldSystemFontOfSize(22)
            
            factsDrLbl.font = UIFont.boldSystemFontOfSize(19)
            
            
        }
        
        tableViewLet.rowHeight = UITableViewAutomaticDimension
        tableViewLet.estimatedRowHeight = 102
        
        arrayForBool = ["0","0","0","0","0","0","0","0","0","0","0"]
        sectionTitleArray = ["Where in the body is  alcohol mostly absorbed?","On average how long does it take for alcohol to be completely absorbed?","What factors may delay alcohol absorption?","How is alcohol distributed?","How do body weight and body fat affect blood alcohol concentration(BAC)?","Who has more water content male or females?","What order kinetics does alcohol elimination follow?","What is the average rate of elimination of alcohol?","How is alcohol metabolized?","What toxic metabolized is formed in the metabolism of alcohol?","When do CYP2E1 enzymes become involved?"]
        
        var tmp1 : NSArray = ["As alcohol travels into the GI tract a small percentage (20-25%) is slowly absorbed through the stomach lining.However,most alcohol absorption (75-80%) oocurs in small intestine of the GI tract."]
        var string1 = sectionTitleArray .objectAtIndex(0) as? String
        [sectionContentDict .setValue(tmp1, forKey:string1! )]
        
        var tmp2 : NSArray = ["Complete alcohol absorption is typically achieved within 30-60 minutes and can take as long as 90 minutes."]
        string1 = sectionTitleArray .objectAtIndex(1) as? String
        [sectionContentDict .setValue(tmp2, forKey:string1! )]
        
        var tmp3 : NSArray = ["Anything that delays the emytying of stomach contents into the small intestine will slow the rate of absorption.For example,diluted drinks take lomger to absorb.Emotional states,shock and medications are also known to affect alcihol absorption.the presence of food in the stomach is the most common reason for delayed absorption of alcohol."]
        string1 = sectionTitleArray .objectAtIndex(2) as? String
        [sectionContentDict .setValue(tmp3, forKey:string1! )]
        
        var tmp4 : NSArray = ["Once absorbed,alcohol is carried by the blood and distributed throughout the body.As blood flows throughout the body organs and tissues,the alcohol diffuses across various membranes into all areas that contain water. Alcohol moves by  simple diffusion from areas of higher concentration to that of a lower concentration."]
        string1 = sectionTitleArray .objectAtIndex(3) as? String
        [sectionContentDict .setValue(tmp4, forKey:string1! )]
        
        var tmp5 : NSArray = ["Alcohol is soluble in water. A BAC is derived from the total amount of alcohol in an individual’s body and divided by his or her total body water and body weight. The higher an individual\'s body weight, the higher their total body water content. When comparing two similar individuals who consumed the same amount of drinks and who weighs less the individual with higher total body water content and body weight will result in a lower BAC compared to the individual who weighs less and therefore has less total body water content.Women on average tend to have less total body water than men. As a result, the alcohol content in a woman\'s body will be more concentrated, less dilute, and yield a higher BAC when consuming the same amount of drinks as men. This explains why women can consume less alcohol than men to achieve a particular BAC. Women also have more body fat than men. Fatty tissue does not contain very much water when compared to lean muscle. This means that there will be less total body water to dilute the alcohol in the body resulting in a higher BAC."]
        
        string1 = sectionTitleArray .objectAtIndex(4) as? String
        [sectionContentDict .setValue(tmp5, forKey:string1! )]
        
        var tmp6 : NSArray = ["Males tend to have more body water than females (contrary to popular belief). On average, about 68% of an average male\'s body weight is due to body water, while around 55% of a female\'s body weight is body water."]
        string1 = sectionTitleArray .objectAtIndex(5) as? String
        [sectionContentDict .setValue(tmp6, forKey:string1! )]
        
        var tmp7 : NSArray = ["Alcohol is traditionally known to follow zero-order kinetics."]
        string1 = sectionTitleArray .objectAtIndex(6) as? String
        [sectionContentDict .setValue(tmp7, forKey:string1! )]
        
        var tmp8 : NSArray = ["The average rate of elimination (combining metabolism, excretion and evaporation) is between 0.015 g/dL to 0.018 g/dL per hour."]
        string1 = sectionTitleArray .objectAtIndex(7) as? String
        [sectionContentDict .setValue(tmp8, forKey:string1! )]
        
        var tmp9 : NSArray = ["Enzymes act on alcohol molecules to change them into other compounds. Alcohol dehydrogenase (ADH) in the liver is the primary enzyme that is responsible for alcohol metabolism. Alcohol dehydrogenase converts ethanol to acetaldehyde. Aldehyde dehydrogenase converts the acetaldehyde to acetate for further elimination via a breakdown to carbon dioxide and water. Alcohol is also excreted unchanged through urine, tears, sweat, semen and saliva."]
        string1 = sectionTitleArray .objectAtIndex(8) as? String
        [sectionContentDict .setValue(tmp9, forKey:string1! )]
        
        var tmp10 : NSArray = ["Acetaldehyde is formed in the metabolism of alcohol. Acute, short-term exposure to acetaldehyde results in effects including irritation of the eyes, skin, and respiratory tract. Symptoms of chronic intoxication of acetaldehyde resemble those of alcoholism. Acetaldehyde is also responsible for the facial flushing (redness) in people who consume alcohol due to a poor ability to"]
        string1 = sectionTitleArray .objectAtIndex(9) as? String
        [sectionContentDict .setValue(tmp10, forKey:string1! )]
        
        var tmp11 : NSArray = ["The CYP2E1 enzyme is responsible for metabolizing alcohol in chronic heavy drinkers. In people who drink alcohol occasionally, CYP2E1 is only responsible for a very small fraction of alcohol metabolism. However, in chronic heavy drinkers CYP2E1 activity has been known to increase ten fold.\nFor a better understanding into the pharmacology of alcohol email us at jkain@cpshealth.com"]
        string1 = sectionTitleArray .objectAtIndex(10) as? String
        [sectionContentDict .setValue(tmp11, forKey:string1! )]
        
        
        
        
        
        factsDrLbl.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.tabBarController?.selectedIndex = 0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sectionTitleArray.count
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if(arrayForBool .objectAtIndex(section).boolValue == true)
        {
            var tps = sectionTitleArray.objectAtIndex(section) as! String
            var count1 = (sectionContentDict.valueForKey(tps)) as! NSArray
            return count1.count
        }
        return 0;
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "ABC"
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 70
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 70))
        headerView.backgroundColor = UIColor(red: 39/255, green: 20/255, blue: 39/255, alpha: 1.0)
        headerView.tag = section
        
        let headerString = UILabel(frame: CGRect(x: 10, y: 10, width: tableView.frame.size.width-10, height: 50)) as UILabel
        headerString.text = sectionTitleArray.objectAtIndex(section) as? String
        headerString.numberOfLines = 3
        headerString.textColor = UIColor.whiteColor()
        headerString.textAlignment = NSTextAlignment.Left
        headerView .addSubview(headerString)
        
        let headerTapped = UITapGestureRecognizer (target: self, action:"sectionHeaderTapped:")
        headerView .addGestureRecognizer(headerTapped)
        
        return headerView
    }
    
    func sectionHeaderTapped(recognizer: UITapGestureRecognizer) {
        println("Tapping working")
        println(recognizer.view?.tag)
        
        var indexPath : NSIndexPath = NSIndexPath(forRow: 0, inSection:(recognizer.view?.tag as Int!)!)
        if (indexPath.row == 0) {
            
            var collapsed = arrayForBool .objectAtIndex(indexPath.section).boolValue
            collapsed       = !collapsed;
            
            arrayForBool .replaceObjectAtIndex(indexPath.section, withObject: collapsed)
            //reload specific section animated
            var range = NSMakeRange(indexPath.section, 1)
            var sectionToReload = NSIndexSet(indexesInRange: range)
            self.tableViewLet .reloadSections(sectionToReload, withRowAnimation:UITableViewRowAnimation.Fade)
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let CellIdentifier = "cell"
        
        var cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier, forIndexPath: indexPath) as! UITableViewCell
        
        var manyCells : Bool = arrayForBool .objectAtIndex(indexPath.section).boolValue
        
        if (!manyCells) {
            
            
        }
        else{
            var content = sectionContentDict .valueForKey(sectionTitleArray.objectAtIndex(indexPath.section) as! String) as! NSArray
            
            
            
            
            cell.textLabel?.text = content .objectAtIndex(indexPath.row) as? String
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.font = UIFont.systemFontOfSize(14)
            cell.textLabel?.textAlignment = NSTextAlignment.Center
            cell.textLabel?.textColor = UIColor.whiteColor()
            
            
        }
        
        return cell
    }
    
    
}
