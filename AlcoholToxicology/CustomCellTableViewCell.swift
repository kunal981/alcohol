//
//  CustomCellTableViewCell.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/29/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class CustomCellTableViewCell: UITableViewCell {
    
  
   
    @IBOutlet var cellView: UIView!
    @IBOutlet var bacLbl: UILabel!
    @IBOutlet var emailLbl: UILabel!
    @IBOutlet var nameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
           
           
            
           
             nameLbl.font = UIFont.boldSystemFontOfSize(18)
             emailLbl.font = UIFont.systemFontOfSize(18)
             bacLbl.font = UIFont.systemFontOfSize(18)
            
            
        }
        

        
        // Configure the view for the selected state
    }
    
}
