//
//  ViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/24/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet var DrinkEstimatorTxtView: UITextView!
    @IBOutlet var retrogradeExtrapolationTxtView: UITextView!
    @IBOutlet var averageMaximumBacTxtView: UITextView!
    @IBOutlet var impairmentTxtView: UITextView!
    @IBOutlet var drinkEstimatorLbl: UILabel!
    @IBOutlet var impairmentLbl: UILabel!
    @IBOutlet var retrogradeExtrapolationLbl: UILabel!
    @IBOutlet var averageMaximumBacLbl: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var behaviourBtn: UIButton!
    @IBOutlet var behaviourView: UIView!
    @IBOutlet var impairmentBtn: UIButton!
    @IBOutlet var impairmentView: UIView!
    @IBOutlet var buildRetro: UIButton!
    
    @IBOutlet var retroView: UIView!
    @IBOutlet var BACView: UIView!
    @IBOutlet var titleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        BACView.layer.borderColor=UIColor.whiteColor().CGColor
        retroView.layer.borderColor=UIColor.whiteColor().CGColor
        impairmentView.layer.borderColor=UIColor.whiteColor().CGColor
        behaviourView.layer.borderColor=UIColor.whiteColor().CGColor
        compatabiity()
        
    }
    
    
    func compatabiity()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Phone)
            
        {
            
            scrollView.frame = self.view.bounds
            scrollView.contentSize.height = view.frame.size.height+330
            
        }
        else
        {
            
            averageMaximumBacTxtView.font = UIFont.systemFontOfSize(19)
            retrogradeExtrapolationTxtView.font = UIFont.systemFontOfSize(19)
            impairmentTxtView.font = UIFont.systemFontOfSize(19)
            DrinkEstimatorTxtView.font = UIFont.systemFontOfSize(19)
            averageMaximumBacLbl.font = UIFont.systemFontOfSize(19)
            retrogradeExtrapolationLbl.font = UIFont.systemFontOfSize(19)
            impairmentLbl.font = UIFont.systemFontOfSize(19)
            drinkEstimatorLbl.font = UIFont.systemFontOfSize(19)
            titleLabel.font = UIFont.systemFontOfSize(22)
            scrollView.frame = self.view.bounds
            scrollView.contentSize.height = view.frame.size.height+460
            
            
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    override func viewWillAppear(animated: Bool) {
        UIApplication.sharedApplication().statusBarHidden = false
        
    }
    
    
}

