//
//  ImpairmentViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/29/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class ImpairmentViewController: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet var bacLbl: UILabel!
    @IBOutlet var caseNameLbl: UILabel!
    var physicalLbl1:UILabel = UILabel()
    var physicalLbl2:UILabel = UILabel()
    var physicalLbl3:UILabel = UILabel()
    var physicalLbl4:UILabel = UILabel()
    var physicalLbl5:UILabel = UILabel()
    var physicalLbl6:UILabel = UILabel()
    
    
    
    var mentalLbl1:UILabel = UILabel()
    var mentalLbl2:UILabel = UILabel()
    var mentalLbl3:UILabel = UILabel()
    var mentalLbl4:UILabel = UILabel()
    
    
    var physicalView = UIView()
    var physicalLbl:UILabel = UILabel()
    var mentalLbl:UILabel = UILabel()
    
    
    
    
    
    var bacImpairment = Float()
    
    @IBOutlet var impairmentTitle: UILabel!
    @IBOutlet var bacTextField: UITextField!
    @IBOutlet var caseNameTextField: UITextField!
    @IBOutlet var bacView: UIView!
    @IBOutlet var caseNameView: UIView!
    @IBOutlet var impairmentLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        caseNameTextField.delegate = self
        bacTextField.delegate = self
        self.navigationItem.hidesBackButton = true
        bacView.layer.borderColor = UIColor.whiteColor().CGColor
        caseNameView.layer.borderColor = UIColor.whiteColor().CGColor
        impairmentLbl.layer.borderColor = UIColor.whiteColor().CGColor
        caseNameTextField.attributedPlaceholder = NSAttributedString(string: "john", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        impairmentTitle.layer.borderColor = UIColor.whiteColor().CGColor
        bacTextField.attributedPlaceholder = NSAttributedString(string: "0.078", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        compatability()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func compatability()
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            impairmentTitle.font = UIFont.boldSystemFontOfSize(22)
            impairmentLbl.font = UIFont.systemFontOfSize(19)
            caseNameLbl.font = UIFont.systemFontOfSize(19)
            bacLbl.font = UIFont.systemFontOfSize(19)
            caseNameTextField.font = UIFont.systemFontOfSize(16)
            bacTextField.font = UIFont.systemFontOfSize(16)
            
            physicalView = UIView(frame: CGRectMake(impairmentLbl.frame.origin.x,impairmentLbl.frame.origin.y - 8  , bacView.frame.size.width, 60))
            physicalView.backgroundColor = UIColor(red: 55/255, green: 35/255, blue: 54/255, alpha: 1.5)
            physicalView.layer.borderWidth = 1.5
            physicalView.layer.cornerRadius = 4
            physicalView.layer.borderColor = UIColor.whiteColor().CGColor
            view.addSubview(physicalView)
            
            
            physicalLbl = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 0,100 , 50))
            physicalLbl.text = "Physical"
            physicalLbl.textColor = UIColor.whiteColor()
            physicalLbl.textAlignment = NSTextAlignment.Left
            
            physicalLbl.font = UIFont.systemFontOfSize(19)
            physicalView.addSubview(physicalLbl)
            
            
            mentalLbl = UILabel(frame: CGRectMake(view.frame.size.width-250, 0,100 , 50))
            mentalLbl.text = "Mental"
            mentalLbl.textColor = UIColor.whiteColor()
            mentalLbl.textAlignment = NSTextAlignment.Center
            mentalLbl.font = UIFont.systemFontOfSize(19)
            physicalView.addSubview(mentalLbl)
            
            
            
            physicalLbl1 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 35,130 , 50))
            physicalLbl1.textColor = UIColor.whiteColor()
            physicalLbl1.numberOfLines = 2
            physicalLbl1.textAlignment = NSTextAlignment.Left
            physicalLbl1.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(physicalLbl1)
            
            physicalLbl2 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 80,130 , 50))
            physicalLbl2.textColor = UIColor.whiteColor()
            physicalLbl2.numberOfLines = 2
            physicalLbl2.textAlignment = NSTextAlignment.Left
            physicalLbl2.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(physicalLbl2)
            
            
            
            physicalLbl3 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 125,130 , 50))
            physicalLbl3.textColor = UIColor.whiteColor()
            physicalLbl3.numberOfLines = 2
            physicalLbl3.textAlignment = NSTextAlignment.Left
            physicalLbl3.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(physicalLbl3)
            
            
            
            physicalLbl4 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 170,130 , 50))
            physicalLbl4.textColor = UIColor.whiteColor()
            physicalLbl4.numberOfLines = 2
            physicalLbl4.textAlignment = NSTextAlignment.Left
            physicalLbl4.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(physicalLbl4)
            
            
            
            
            mentalLbl1 = UILabel(frame: CGRectMake(view.frame.size.width-235, 35,130 , 50))
            mentalLbl1.textColor = UIColor.whiteColor()
            mentalLbl1.numberOfLines = 2
            mentalLbl1.textAlignment = NSTextAlignment.Left
            mentalLbl1.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(mentalLbl1)
            
            mentalLbl2 = UILabel(frame: CGRectMake(view.frame.size.width-235, 80,130 , 50))
            mentalLbl2.textColor = UIColor.whiteColor()
            mentalLbl2.numberOfLines = 2
            mentalLbl2.textAlignment = NSTextAlignment.Left
            mentalLbl2.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(mentalLbl2)
            
            
            
            mentalLbl3 = UILabel(frame: CGRectMake(view.frame.size.width-235, 125,130 , 50))
            mentalLbl3.textColor = UIColor.whiteColor()
            mentalLbl3.numberOfLines = 2
            mentalLbl3.textAlignment = NSTextAlignment.Left
            mentalLbl3.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(mentalLbl3)
            
            mentalLbl4 = UILabel(frame: CGRectMake(view.frame.size.width-235, 170,130 , 50))
            mentalLbl4.textColor = UIColor.whiteColor()
            mentalLbl4.numberOfLines = 2
            mentalLbl4.textAlignment = NSTextAlignment.Left
            mentalLbl4.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(mentalLbl4)
            
            
            
        }
            
            
        else
        {
            
            
            
            
            physicalView = UIView(frame: CGRectMake(impairmentLbl.frame.origin.x,impairmentLbl.frame.origin.y - 20  , bacView.frame.size.width, 60))
            physicalView.backgroundColor = UIColor(red: 55/255, green: 35/255, blue: 54/255, alpha: 1.5)
            physicalView.layer.borderWidth = 1.5
            physicalView.layer.cornerRadius = 4
            physicalView.layer.borderColor = UIColor.whiteColor().CGColor
            view.addSubview(physicalView)
            
            
            
            
            physicalLbl = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 0,100 , 50))
            physicalLbl.text = "Physical"
            physicalLbl.textColor = UIColor.whiteColor()
            physicalLbl.textAlignment = NSTextAlignment.Left
            physicalView.addSubview(physicalLbl)
            
            
            mentalLbl = UILabel(frame: CGRectMake(view.frame.size.width-160, 0,100 , 50))
            mentalLbl.text = "Mental"
            mentalLbl.textColor = UIColor.whiteColor()
            mentalLbl.textAlignment = NSTextAlignment.Center
            physicalView.addSubview(mentalLbl)
            
            
            
            physicalLbl1 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 35,100 , 50))
            physicalLbl1.textColor = UIColor.whiteColor()
            physicalLbl1.numberOfLines = 2
            physicalLbl1.textAlignment = NSTextAlignment.Left
            physicalLbl1.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(physicalLbl1)
            
            physicalLbl2 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 70,100 , 50))
            physicalLbl2.textColor = UIColor.whiteColor()
            physicalLbl2.numberOfLines = 2
            physicalLbl2.textAlignment = NSTextAlignment.Left
            physicalLbl2.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(physicalLbl2)
            
            
            
            physicalLbl3 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 105,100 , 50))
            physicalLbl3.textColor = UIColor.whiteColor()
            physicalLbl3.numberOfLines = 2
            physicalLbl3.textAlignment = NSTextAlignment.Left
            physicalLbl3.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(physicalLbl3)
            
            
            
            physicalLbl4 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 140,100 , 50))
            physicalLbl4.textColor = UIColor.whiteColor()
            physicalLbl4.numberOfLines = 2
            physicalLbl4.textAlignment = NSTextAlignment.Left
            physicalLbl4.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(physicalLbl4)
            
            
            
            
            mentalLbl1 = UILabel(frame: CGRectMake(view.frame.size.width-160, 35,100 , 50))
            mentalLbl1.textColor = UIColor.whiteColor()
            mentalLbl1.numberOfLines = 2
            mentalLbl1.textAlignment = NSTextAlignment.Left
            mentalLbl1.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(mentalLbl1)
            
            mentalLbl2 = UILabel(frame: CGRectMake(view.frame.size.width-160, 70,100 , 50))
            mentalLbl2.textColor = UIColor.whiteColor()
            mentalLbl2.numberOfLines = 2
            mentalLbl2.textAlignment = NSTextAlignment.Left
            mentalLbl2.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(mentalLbl2)
            
            
            
            mentalLbl3 = UILabel(frame: CGRectMake(view.frame.size.width-160, 105,100 , 50))
            mentalLbl3.textColor = UIColor.whiteColor()
            mentalLbl3.numberOfLines = 2
            mentalLbl3.textAlignment = NSTextAlignment.Left
            mentalLbl3.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(mentalLbl3)
            
            mentalLbl4 = UILabel(frame: CGRectMake(view.frame.size.width-160, 140,100 , 50))
            mentalLbl4.textColor = UIColor.whiteColor()
            mentalLbl4.numberOfLines = 2
            mentalLbl4.textAlignment = NSTextAlignment.Left
            mentalLbl4.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(mentalLbl4)
        }
        
        
        
        
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        
        
        if  caseNameTextField.resignFirstResponder()
        {
            bacTextField.becomeFirstResponder()
            
            return false
        }
        
        if bacTextField.resignFirstResponder()
        {
            bacImpairment = NSString(format: "%@", bacTextField.text).floatValue
            println(bacImpairment)
            
            if caseNameTextField.text == "" || bacTextField.text == ""
                
            {
                let alert = UIAlertView(title: "Alert", message: "Empty Text ", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
                
                physicalView.frame.size.height = 60
                physicalLbl1.text = ""
                physicalLbl2.text = ""
                physicalLbl3.text = ""
                physicalLbl4.text = ""
                
                mentalLbl1.text = ""
                mentalLbl2.text = ""
                mentalLbl3.text = ""
                mentalLbl4.text = ""
                
                
                
                
            }
                
            else
            {
                
                if bacImpairment <= 0.060 && bacImpairment >= 0.010
                {
                    
                    
                    physicalLbl1.text = "> Concen-tration"
                    physicalLbl2.text = "> Thought"
                    physicalLbl3.text = "> Judgement"
                    physicalLbl4.text = "> Coordination"
                    
                    mentalLbl1.text = "> Lowered Alertness"
                    mentalLbl2.text = "> Disinhibition"
                    mentalLbl3.text = "> Sense of Well-Being"
                    mentalLbl4.text = "> Relaxation"
                    
                    
                    
                    if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
                    {
                        physicalView.frame.size.height = 220
                        
                        
                    }
                    else
                    {
                        physicalView.frame.size.height = 200
                        
                    }
                    
                    
                    
                }
                    
                else if bacImpairment <= 0.10 && bacImpairment > 0.060
                    
                {
                    
                    physicalView.frame.size.height = 140
                    physicalLbl1.text = "> Breathing"
                    physicalLbl2.text = "> Heart Rate"
                    physicalLbl3.text = ""
                    physicalLbl4.text = ""
                    
                    mentalLbl1.text = "> Unconsc-iousness"
                    mentalLbl2.text = "> Death"
                    mentalLbl3.text = ""
                    mentalLbl4.text = ""
                    
                    
                }
                    
                else if bacImpairment <= 0.20 && bacImpairment > 0.10
                    
                {
                    
                    physicalLbl1.text = "> Reaction Time"
                    physicalLbl2.text = "> Gross Motor Control"
                    physicalLbl3.text = "> Staggering"
                    physicalLbl4.text = "> Slurred Speech"
                    
                    mentalLbl1.text = "> Over-Expression"
                    mentalLbl2.text = "> Emotional Swings"
                    mentalLbl3.text = "> Angry or Sad"
                    mentalLbl4.text = "> Boisterous"
                    
                    
                    
                    if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
                    {
                        physicalView.frame.size.height = 220
                        
                        
                    }
                    else
                    {
                        physicalView.frame.size.height = 200
                        
                    }
                    
                    
                }
                    
                else if bacImpairment <= 0.29 && bacImpairment > 0.20
                    
                {
                    physicalView.frame.size.height = 180
                    physicalLbl1.text = "> Severe Motor Impairment"
                    physicalLbl2.text = "> Loss of Consciousness"
                    physicalLbl3.text = "> Memory Blackout"
                    physicalLbl4.text = ""
                    
                    mentalLbl1.text = "> Stupor"
                    mentalLbl2.text = "> Loss of Understanding"
                    mentalLbl3.text = "> Impaired Sensation"
                    mentalLbl4.text = ""
                    
                }
                    
                else if bacImpairment <= 0.39 && bacImpairment > 0.29
                {
                    
                    physicalView.frame.size.height = 180
                    physicalLbl1.text = "> Bladder Function"
                    physicalLbl2.text = "> Breathing"
                    physicalLbl3.text = "> Heart Rate"
                    physicalLbl4.text = ""
                    
                    mentalLbl1.text = "> Severe Depression"
                    mentalLbl2.text = "> Unconsc-iousness"
                    mentalLbl3.text = "> Death"
                    mentalLbl4.text = ""
                    
                }
                else if bacImpairment > 0.40
                {
                    
                    physicalView.frame.size.height = 140
                    physicalLbl1.text = "> Breathing"
                    physicalLbl2.text = "> Heart Rate"
                    physicalLbl3.text = ""
                    physicalLbl4.text = ""
                    
                    mentalLbl1.text = "> Unconsc-iousness"
                    mentalLbl2.text = "> Death"
                    mentalLbl3.text = ""
                    mentalLbl4.text = ""            }
                else
                {
                    physicalView.frame.size.height = 100
                    physicalLbl1.text = "> No Information"
                    physicalLbl2.text = ""
                    physicalLbl3.text = ""
                    physicalLbl4.text = ""
                    
                    
                    mentalLbl1.text = "> No Information"
                    mentalLbl2.text = ""
                    mentalLbl3.text = ""
                    mentalLbl4.text = ""
                }
            }
            
            bacTextField.text = ""
            
            
            
            
            
        }
        return true
        
        
    }
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
