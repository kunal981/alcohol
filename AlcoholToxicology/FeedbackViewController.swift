//
//  FeedbackViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/25/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import MessageUI
class FeedbackViewController: UIViewController, UITextFieldDelegate,UITextViewDelegate,MFMailComposeViewControllerDelegate {
    
    
    @IBOutlet var messagelbl: UILabel!
    @IBOutlet var toLbl: UILabel!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var feedbackTxtViewTitle: UITextView!
    @IBOutlet var messsageTxtView: UITextView!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var nameTxtFiled: UITextField!
    @IBOutlet var feedbackCancel: UIButton!
    @IBOutlet var feedbackSend: UIButton!
    @IBOutlet var feedbackView: UIView!
    @IBOutlet var feedbackLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let paddingViewName = UIView(frame: CGRectMake(0, 0, 15, nameTxtFiled.frame.size.height))
        nameTxtFiled.leftView = paddingViewName
        nameTxtFiled.leftViewMode = UITextFieldViewMode.Always
        
        
        let paddingViewEmail = UIView(frame: CGRectMake(0, 0, 15, emailTxtField.frame.size.height))
        emailTxtField.leftView = paddingViewEmail
        emailTxtField.leftViewMode = UITextFieldViewMode.Always
        
        nameTxtFiled.delegate = self
        emailTxtField.delegate = self
        messsageTxtView.delegate = self
        
        feedbackCancel.layer.borderColor = UIColor.whiteColor().CGColor
        feedbackSend.layer.borderColor = UIColor.whiteColor().CGColor
        feedbackView.layer.borderColor = UIColor.whiteColor().CGColor
        messsageTxtView.layer.borderColor = UIColor.whiteColor().CGColor
        emailTxtField.layer.borderColor = UIColor.whiteColor().CGColor
        nameTxtFiled.layer.borderColor = UIColor.whiteColor().CGColor
        
        nameTxtFiled.attributedPlaceholder = NSAttributedString(string: "Enter your name", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        compatability()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func compatability()
    {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
            
        {
            
            nameLbl.font = UIFont.systemFontOfSize(18)
            toLbl.font = UIFont.systemFontOfSize(18)
            messagelbl.font = UIFont.systemFontOfSize(18)
            feedbackSend.titleLabel?.font=UIFont.boldSystemFontOfSize(18)
            feedbackCancel.titleLabel?.font = UIFont.boldSystemFontOfSize(18)
            feedbackTxtViewTitle.font = UIFont.systemFontOfSize(19)
            feedbackLabel.font = UIFont.boldSystemFontOfSize(22)
            
            nameTxtFiled.font = UIFont.systemFontOfSize(16)
            emailTxtField.font = UIFont.systemFontOfSize(16)
            messsageTxtView.font = UIFont.systemFontOfSize(16)
            
        }
        
        
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.tabBarController?.selectedIndex = 0
    }
    
    func textViewDidBeginEditing(textView: UITextView)
    {
        if textView.text ==  "Enter your message"
        {
            textView.text = ""
        }
        
        
    }
    func textViewDidEndEditing(textView: UITextView)
    {
        if textView.text == ""
        {
            textView.text = "Enter your message"
        }
        
        
        
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        messsageTxtView.resignFirstResponder()
        nameTxtFiled.resignFirstResponder()
        emailTxtField.resignFirstResponder()
        
    }
    
    
    
    
    @IBAction func sendBtn(sender: AnyObject)
    {
        if (nameTxtFiled.text == "" || emailTxtField.text == "" || messsageTxtView.text == "")
        {
            var alert = UIAlertView(title: "Alert", message: "Enter detail first", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
        else
        {
            
            
            
            
            var emailTitle = ""
            var messageBody = messsageTxtView.text
            var toRecipents = [emailTxtField.text]
            var mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setMessageBody(messageBody, isHTML: false)
            mc.setToRecipients(toRecipents)
            
            self.presentViewController(mc, animated: true, completion: nil)
        }
        
        
    }
    
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError)
    {
        
        var refreshAlert1 = UIAlertController(title: "Mail sent", message: "You request has been sent successfully", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert1.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
            //println("Handle Ok logic here")
            
        }))
        
        
        
        
        
        var refreshAlert2 = UIAlertController(title: "Mail couldn't sent", message: "Your request could not be processed", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert2.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
            //println("Handle Ok logic here")
        }))
        
        
        
        
        var showAlert = true
        var alertView: UIAlertController!
        
        switch result.value {
        case MFMailComposeResultCancelled.value:
            println("Mail cancelled")
            showAlert = false
            
        case MFMailComposeResultSaved.value:
            println("Mail saved")
            showAlert = false
            
        case MFMailComposeResultSent.value:
            alertView = refreshAlert1
            
        case MFMailComposeResultFailed.value:
            alertView = refreshAlert2
            
        default:
            showAlert = false
            break
        }
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if showAlert
        {
            presentViewController(alertView, animated: true, completion: nil)
        }
        
        
        
        
        
    }
    
    @IBAction func cancelBtn(sender: AnyObject)
    {
        //        nameTxtFiled.text = ""
        //        emailTxtField.text = ""
        //        messsageTxtView.text = ""
    }
}