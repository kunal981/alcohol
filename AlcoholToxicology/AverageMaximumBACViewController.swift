//
//  AverageMaximumBACViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/25/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


class AverageMaximumBACViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    
    
    
    var gender:NSArray = ["Male","Female"]
    
    var strText = NSString()
    var sizeHeight = UIScreen.mainScreen().bounds.height
    @IBOutlet var drinkerProfileLbl: UILabel!
    @IBOutlet var weightLbl: UILabel!
    @IBOutlet var caseNameLbl: UILabel!
    @IBOutlet var genderTextField: UITextField!
    @IBOutlet var weightTextField: UITextField!
    
    @IBOutlet var genderTitleLbl: UILabel!
    @IBOutlet var tableViewOutlet: UITableView!
    
    @IBOutlet var caseNameTextField: UITextField!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var weightView: UIView!
    @IBOutlet var genderView: UIView!
    @IBOutlet var caseNameView: UIView!
    @IBOutlet var avergeMaxBACLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if sizeHeight == 667
        {
            
            
            var line = UIView(frame: CGRectMake(30, drinkerProfileLbl.frame.origin.y-6, 100, 1))
            line.backgroundColor = UIColor.whiteColor()
            
            self.view.addSubview(line)
            
            var line2 = UIView(frame: CGRectMake(245, drinkerProfileLbl.frame.origin.y-6, 100, 1))
            line2.backgroundColor = UIColor.whiteColor()
            self.view.addSubview(line2)
        }
            
            
        else if sizeHeight == 736
        {
            var line = UIView(frame: CGRectMake(30, drinkerProfileLbl.frame.origin.y-4, 120, 1))
            line.backgroundColor = UIColor.whiteColor()
            
            self.view.addSubview(line)
            
            var line2 = UIView(frame: CGRectMake(265, drinkerProfileLbl.frame.origin.y-4, 120, 1))
            line2.backgroundColor = UIColor.whiteColor()
            self.view.addSubview(line2)
            
        }
        else if sizeHeight == 568
        {
            
            var line = UIView(frame: CGRectMake(30, drinkerProfileLbl.frame.origin.y-8, 75, 1))
            line.backgroundColor = UIColor.whiteColor()
            
            self.view.addSubview(line)
            
            var line2 = UIView(frame: CGRectMake(215, drinkerProfileLbl.frame.origin.y-8, 75, 1))
            line2.backgroundColor = UIColor.whiteColor()
            self.view.addSubview(line2)
        }
        else if sizeHeight == 480
            
        {
            var line = UIView(frame: CGRectMake(30, drinkerProfileLbl.frame.origin.y-10, 73, 1))
            line.backgroundColor = UIColor.whiteColor()
            
            self.view.addSubview(line)
            
            var line2 = UIView(frame: CGRectMake(215, drinkerProfileLbl.frame.origin.y-10, 73, 1))
            line2.backgroundColor = UIColor.whiteColor()
            self.view.addSubview(line2)
        }
        else
        {
            
            
            drinkerProfileLbl.font=UIFont.systemFontOfSize(20)
            
            var line = UIView(frame: CGRectMake(40, drinkerProfileLbl.frame.origin.y, 270, 1))
            line.backgroundColor = UIColor.whiteColor()
            
            self.view.addSubview(line)
            
            var line2 = UIView(frame: CGRectMake(460, drinkerProfileLbl.frame.origin.y, 270, 1))
            line2.backgroundColor = UIColor.whiteColor()
            self.view.addSubview(line2)
        }
        
        
        
        
        
        let paddingViewName = UIView(frame: CGRectMake(0, 0, 15, caseNameTextField.frame.size.height))
        caseNameTextField.leftView = paddingViewName
        caseNameTextField.leftViewMode = UITextFieldViewMode.Always
        
        
        let paddingViewWeight = UIView(frame: CGRectMake(0, 0, 15, weightTextField.frame.size.height))
        weightTextField.leftView = paddingViewWeight
        weightTextField.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewGender = UIView(frame: CGRectMake(0, 0, 15, genderTextField.frame.size.height))
        genderTextField.leftView = paddingViewGender
        genderTextField.leftViewMode = UITextFieldViewMode.Always
        
        
        tableViewOutlet.frame = CGRectMake(genderView.frame.origin.x + genderTitleLbl.frame.size.width,genderView.frame.origin.y + 55, genderTextField.frame.size.width, 93)
        
        self.tableViewOutlet.hidden = true
        self.navigationItem.hidesBackButton = true
        caseNameTextField.delegate = self
        
        weightTextField.delegate = self
        weightView.layer.borderColor = UIColor.whiteColor().CGColor
        genderView.layer.borderColor = UIColor.whiteColor().CGColor
        caseNameView.layer.borderColor = UIColor.whiteColor().CGColor
        avergeMaxBACLabel.layer.borderColor = UIColor.whiteColor().CGColor
        nextBtn.layer.borderColor = UIColor.whiteColor().CGColor
        
        caseNameTextField.attributedPlaceholder = NSAttributedString(string: "John", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        tableViewOutlet.layer.borderColor = UIColor.grayColor().CGColor
        
        weightTextField.attributedPlaceholder = NSAttributedString(string: "152 lbs", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        compatability()
    }
    
    func compatability()
    {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            avergeMaxBACLabel.font = UIFont.boldSystemFontOfSize(22)
            caseNameLbl.font = UIFont.systemFontOfSize(20)
            genderTitleLbl.font = UIFont.systemFontOfSize(20)
            weightLbl.font = UIFont.systemFontOfSize(20)
            nextBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(22)
            tableViewOutlet.frame.origin.y = tableViewOutlet.frame.origin.y + 50
            caseNameTextField.font = UIFont.systemFontOfSize(16)
            weightTextField.font = UIFont.systemFontOfSize(16)
            genderTextField.font = UIFont.systemFontOfSize(16)
        }
        
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        caseNameTextField.text = ""
        weightTextField.text = ""
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        
        
        textField.resignFirstResponder()
        
        
        return true
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return gender.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
        
        cell.textLabel?.text = gender[indexPath.row] as? String
        cell.textLabel?.textColor = UIColor.whiteColor()
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            
            cell.backgroundColor = UIColor.blackColor()
            
        }
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var str: AnyObject = gender[indexPath.row]
        
        
        genderTextField.text = str as? String
        
        self.tableViewOutlet.hidden = true
        
    }
    @IBAction func btnGender(sender: AnyObject)
    {
        self.tableViewOutlet.hidden = false
    }
    
    
    
    @IBAction func nextBtn(sender: AnyObject)
    {
        if (caseNameTextField.text == "" || genderTextField.text == "" || weightTextField.text == "" )
        {
            var alert = UIAlertView(title: "Alert", message: "Required all field to enter", delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
        }
        else
        {
            if genderTextField.text == "Male"
            {
                r = 0.68
                
            }
            if genderTextField.text == "Female"
            {
                r = 0.55
                
            }
            
            
            
            
            
            strWeight = weightTextField.text
            strDescription = caseNameTextField.text
            stringLabelCalculate = caseNameTextField.text
            var calculate = storyboard?.instantiateViewControllerWithIdentifier("calculate") as! CalculateDrinkingViewController
            self.navigationController?.pushViewController(calculate, animated: true)
            
            
        }
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.tableViewOutlet.hidden = true
        weightTextField.resignFirstResponder()
        caseNameTextField.resignFirstResponder()
        
    }
    func textFieldDidEndEditing(textField: UITextField)
    {
        if textField == weightTextField
        {
            
            if weightTextField.text.isEmpty
                
            {
                weightTextField.text = ""
            }
                
            else
            {
                weightTextField.text = NSString(format: "%@ lbs.", weightTextField.text) as String
            }
        }
    }
    func textFieldDidBeginEditing(textField: UITextField)
        
    {
        tableViewOutlet.hidden = true
    }
    
}
