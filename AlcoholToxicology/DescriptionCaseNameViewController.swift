//
//  DescriptionCaseNameViewController.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 6/26/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


var condition = Bool()
var bacFloat = Float()
var strBACLabel = NSString()
var strDescription = NSString()
class DescriptionCaseNameViewController: UIViewController {
    
    
    
    
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var bacLeftLbl: UILabel!
    var physicalLbl1:UILabel = UILabel()
    var physicalLbl2:UILabel = UILabel()
    var physicalLbl3:UILabel = UILabel()
    var physicalLbl4:UILabel = UILabel()
    var physicalLbl5:UILabel = UILabel()
    var physicalLbl6:UILabel = UILabel()
    
    
    
    var mentalLbl1:UILabel = UILabel()
    var mentalLbl2:UILabel = UILabel()
    var mentalLbl3:UILabel = UILabel()
    var mentalLbl4:UILabel = UILabel()
    
    var physicalView = UIView()
    var physicalLbl = UILabel()
    var mentalLbl = UILabel()
    
    
    @IBOutlet var viewResult: UIView!
    @IBOutlet var bacLabel: UILabel!
    @IBOutlet var descriptiontextView: UITextView!
    @IBOutlet var caseNameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if condition == true
        {
            bacLabel.text = NSString(format: "%.2f g/dl", bacFloat) as String
            
        }
        else
        {
            
            bacLabel.text = strBACLabel as String
        }
        
        caseNameLabel.text = strDescription as String
        self.navigationItem.hidesBackButton = true
        caseNameLabel.layer.borderColor = UIColor.whiteColor().CGColor
        viewResult.layer.borderColor = UIColor.whiteColor().CGColor
        descriptiontextView.layer.borderColor = UIColor.whiteColor().CGColor
        
        compatability()
        
        calculationImpairemnet()
        
    }
    
    
    func calculationImpairemnet()
    {
        
        println(bacLabel)
        var bacImpairment = NSString(format: "%@", bacLabel.text!).floatValue
        println(bacImpairment)
        
        
        
        if bacImpairment <= 0.060 && bacImpairment >= 0.010
        {
            
            physicalView.frame.size.height = 200
            physicalLbl1.text = "> Concen-tration"
            physicalLbl2.text = "> Thought"
            physicalLbl3.text = "> Judgement"
            physicalLbl4.text = "> Coordination"
            
            mentalLbl1.text = "> Lowered Alertness"
            mentalLbl2.text = "> Disinhibition"
            mentalLbl3.text = "> Sense of Well-Being"
            mentalLbl4.text = "> Relaxation"
            
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
            {
                physicalView.frame.size.height = 220
                
                
            }
            else
            {
                physicalView.frame.size.height = 200
                
            }
            
            
            
        }
            
        else if bacImpairment <= 0.10 && bacImpairment > 0.060
            
        {
            
            physicalView.frame.size.height = 140
            physicalLbl1.text = "> Breathing"
            physicalLbl2.text = "> Heart Rate"
            physicalLbl3.text = ""
            physicalLbl4.text = ""
            
            mentalLbl1.text = "> Unconsc-iousness"
            mentalLbl2.text = "> Death"
            mentalLbl3.text = ""
            mentalLbl4.text = ""
            
            
        }
            
        else if bacImpairment <= 0.20 && bacImpairment > 0.10
            
        {
            physicalView.frame.size.height = 200
            physicalLbl1.text = "> Reaction Time"
            physicalLbl2.text = "> Gross Motor Control"
            physicalLbl3.text = "> Staggering"
            physicalLbl4.text = "> Slurred Speech"
            
            mentalLbl1.text = "> Over-Expression"
            mentalLbl2.text = "> Emotional Swings"
            mentalLbl3.text = "> Angry or Sad"
            mentalLbl4.text = "> Boisterous"
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
            {
                physicalView.frame.size.height = 220
                
                
            }
            else
            {
                physicalView.frame.size.height = 200
                
            }
            
            
        }
            
        else if bacImpairment <= 0.29 && bacImpairment > 0.20
            
        {
            physicalView.frame.size.height = 180
            physicalLbl1.text = "> Severe Motor Impairment"
            physicalLbl2.text = "> Loss of Consciousness"
            physicalLbl3.text = "> Memory Blackout"
            physicalLbl4.text = ""
            
            mentalLbl1.text = "> Stupor"
            mentalLbl2.text = "> Loss of Understanding"
            mentalLbl3.text = "> Impaired Sensation"
            mentalLbl4.text = ""
            
        }
            
        else if bacImpairment <= 0.39 && bacImpairment > 0.29
        {
            
            physicalView.frame.size.height = 180
            physicalLbl1.text = "> Bladder Function"
            physicalLbl2.text = "> Breathing"
            physicalLbl3.text = "> Heart Rate"
            physicalLbl4.text = ""
            
            mentalLbl1.text = "> Severe Depression"
            mentalLbl2.text = "> Unconsc-iousness"
            mentalLbl3.text = "> Death"
            mentalLbl4.text = ""
            
        }
        else if bacImpairment > 0.40
        {
            
            physicalView.frame.size.height = 140
            physicalLbl1.text = "> Breathing"
            physicalLbl2.text = "> Heart Rate"
            physicalLbl3.text = ""
            physicalLbl4.text = ""
            
            mentalLbl1.text = "> Unconsc-iousness"
            mentalLbl2.text = "> Death"
            mentalLbl3.text = ""
            mentalLbl4.text = ""            }
        else
        {
            physicalView.frame.size.height = 100
            physicalLbl1.text = "> No Information"
            physicalLbl2.text = ""
            physicalLbl3.text = ""
            physicalLbl4.text = ""
            
            
            mentalLbl1.text = "> No Information"
            mentalLbl2.text = ""
            mentalLbl3.text = ""
            mentalLbl4.text = ""
        }
        
    }
    
    func compatability()
    {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad
        {
            caseNameLabel.font = UIFont.boldSystemFontOfSize(22)
            bacLabel.font = UIFont.systemFontOfSize(18)
            bacLeftLbl.font = UIFont.systemFontOfSize(18)
            descriptionLbl.font = UIFont.systemFontOfSize(18)
            descriptiontextView.font = UIFont.systemFontOfSize(22)
            descriptiontextView.frame.size.height = 150
            
            
            
            physicalView = UIView(frame: CGRectMake(viewResult.frame.origin.x,viewResult.frame.origin.y + 50  , viewResult.frame.size.width, 60))
            physicalView.backgroundColor = UIColor(red: 55/255, green: 35/255, blue: 54/255, alpha: 1.5)
            physicalView.layer.borderWidth = 1.5
            physicalView.layer.cornerRadius = 4
            physicalView.layer.borderColor = UIColor.whiteColor().CGColor
            view.addSubview(physicalView)
            
            physicalLbl = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 0,100 , 50))
            physicalLbl.text = "Physical"
            physicalLbl.textColor = UIColor.whiteColor()
            physicalLbl.textAlignment = NSTextAlignment.Left
            physicalLbl.font = UIFont.systemFontOfSize(19)
            physicalView.addSubview(physicalLbl)
            
            mentalLbl = UILabel(frame: CGRectMake(view.frame.size.width-220, 0,100 , 50))
            mentalLbl.text = "Mental"
            mentalLbl2.backgroundColor=UIColor.redColor();
            mentalLbl.textColor = UIColor.whiteColor()
            mentalLbl.textAlignment = NSTextAlignment.Center
            mentalLbl.font = UIFont.systemFontOfSize(19)
            physicalView.addSubview(mentalLbl)
            
            
            physicalLbl1 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 35,130 , 50))
            physicalLbl1.textColor = UIColor.whiteColor()
            physicalLbl1.numberOfLines = 2
            physicalLbl1.textAlignment = NSTextAlignment.Left
            physicalLbl1.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(physicalLbl1)
            
            physicalLbl2 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 80,130 , 50))
            physicalLbl2.textColor = UIColor.whiteColor()
            physicalLbl2.numberOfLines = 2
            physicalLbl2.textAlignment = NSTextAlignment.Left
            physicalLbl2.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(physicalLbl2)
            
            
            
            physicalLbl3 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 125,130 , 50))
            physicalLbl3.textColor = UIColor.whiteColor()
            physicalLbl3.numberOfLines = 2
            physicalLbl3.textAlignment = NSTextAlignment.Left
            physicalLbl3.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(physicalLbl3)
            
            
            
            physicalLbl4 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 170,130 , 50))
            physicalLbl4.textColor = UIColor.whiteColor()
            physicalLbl4.numberOfLines = 2
            physicalLbl4.textAlignment = NSTextAlignment.Left
            physicalLbl4.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(physicalLbl4)
            
            
            
            
            mentalLbl1 = UILabel(frame: CGRectMake(view.frame.size.width-200, 35,130 , 50))
            mentalLbl1.textColor = UIColor.whiteColor()
            mentalLbl1.numberOfLines = 2
            mentalLbl1.textAlignment = NSTextAlignment.Left
            mentalLbl1.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(mentalLbl1)
            
            mentalLbl2 = UILabel(frame: CGRectMake(view.frame.size.width-200, 80,130 , 50))
            mentalLbl2.textColor = UIColor.whiteColor()
            mentalLbl2.numberOfLines = 2
            mentalLbl2.textAlignment = NSTextAlignment.Left
            mentalLbl2.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(mentalLbl2)
            
            
            
            mentalLbl3 = UILabel(frame: CGRectMake(view.frame.size.width-200, 125,130 , 50))
            mentalLbl3.textColor = UIColor.whiteColor()
            mentalLbl3.numberOfLines = 2
            mentalLbl3.textAlignment = NSTextAlignment.Left
            mentalLbl3.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(mentalLbl3)
            
            mentalLbl4 = UILabel(frame: CGRectMake(view.frame.size.width-200, 170,130 , 50))
            mentalLbl4.textColor = UIColor.whiteColor()
            mentalLbl4.numberOfLines = 2
            mentalLbl4.textAlignment = NSTextAlignment.Left
            mentalLbl4.font = UIFont.systemFontOfSize(16)
            physicalView.addSubview(mentalLbl4)
            
            
            
            
            
        }
            
            
        else
        {
            physicalView = UIView(frame: CGRectMake(viewResult.frame.origin.x,viewResult.frame.origin.y + 30  , viewResult.frame.size.width, 60))
            physicalView.backgroundColor = UIColor(red: 55/255, green: 35/255, blue: 54/255, alpha: 1.5)
            physicalView.layer.borderWidth = 1.5
            physicalView.layer.cornerRadius = 4
            physicalView.layer.borderColor = UIColor.whiteColor().CGColor
            view.addSubview(physicalView)
            
            physicalLbl = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 0,100 , 50))
            physicalLbl.text = "Physical"
            physicalLbl.textColor = UIColor.whiteColor()
            physicalLbl.textAlignment = NSTextAlignment.Left
            physicalView.addSubview(physicalLbl)
            
            mentalLbl = UILabel(frame: CGRectMake(view.frame.size.width-160, 0,100 , 50))
            mentalLbl.text = "Mental"
            mentalLbl2.backgroundColor=UIColor.redColor();
            mentalLbl.textColor = UIColor.whiteColor()
            mentalLbl.textAlignment = NSTextAlignment.Center
            physicalView.addSubview(mentalLbl)
            
            
            physicalLbl1 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 35,100 , 50))
            physicalLbl1.textColor = UIColor.whiteColor()
            physicalLbl1.numberOfLines = 2
            physicalLbl1.textAlignment = NSTextAlignment.Left
            physicalLbl1.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(physicalLbl1)
            
            physicalLbl2 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 70,100 , 50))
            physicalLbl2.textColor = UIColor.whiteColor()
            physicalLbl2.numberOfLines = 2
            physicalLbl2.textAlignment = NSTextAlignment.Left
            physicalLbl2.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(physicalLbl2)
            
            
            
            physicalLbl3 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 105,100 , 50))
            physicalLbl3.textColor = UIColor.whiteColor()
            physicalLbl3.numberOfLines = 2
            physicalLbl3.textAlignment = NSTextAlignment.Left
            physicalLbl3.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(physicalLbl3)
            
            
            
            physicalLbl4 = UILabel(frame: CGRectMake(physicalView.frame.origin.x, 140,100 , 50))
            physicalLbl4.textColor = UIColor.whiteColor()
            physicalLbl4.numberOfLines = 2
            physicalLbl4.textAlignment = NSTextAlignment.Left
            physicalLbl4.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(physicalLbl4)
            
            
            
            
            mentalLbl1 = UILabel(frame: CGRectMake(view.frame.size.width-160, 35,100 , 50))
            mentalLbl1.textColor = UIColor.whiteColor()
            mentalLbl1.numberOfLines = 2
            mentalLbl1.textAlignment = NSTextAlignment.Left
            mentalLbl1.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(mentalLbl1)
            
            mentalLbl2 = UILabel(frame: CGRectMake(view.frame.size.width-160, 70,100 , 50))
            mentalLbl2.textColor = UIColor.whiteColor()
            mentalLbl2.numberOfLines = 2
            mentalLbl2.textAlignment = NSTextAlignment.Left
            mentalLbl2.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(mentalLbl2)
            
            
            
            mentalLbl3 = UILabel(frame: CGRectMake(view.frame.size.width-160, 105,100 , 50))
            mentalLbl3.textColor = UIColor.whiteColor()
            mentalLbl3.numberOfLines = 2
            mentalLbl3.textAlignment = NSTextAlignment.Left
            mentalLbl3.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(mentalLbl3)
            
            mentalLbl4 = UILabel(frame: CGRectMake(view.frame.size.width-160, 140,100 , 50))
            mentalLbl4.textColor = UIColor.whiteColor()
            mentalLbl4.numberOfLines = 2
            mentalLbl4.textAlignment = NSTextAlignment.Left
            mentalLbl4.font = UIFont.systemFontOfSize(14)
            physicalView.addSubview(mentalLbl4)
            
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
        
    }
}
