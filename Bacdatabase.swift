//
//  Bacdatabase.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 7/7/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import Foundation
import CoreData

class Bacdatabase: NSManagedObject {

    @NSManaged var averageBac: String
    @NSManaged var bacResult: NSNumber
    @NSManaged var caseName: String

}
