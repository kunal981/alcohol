//
//  Customdrink.swift
//  AlcoholToxicology
//
//  Created by mrinal khullar on 7/4/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import Foundation
import CoreData

class Customdrink: NSManagedObject {

    @NSManaged var drinkName: String
    @NSManaged var alcoholPercent: String
    @NSManaged var volumeDrink: String

}
